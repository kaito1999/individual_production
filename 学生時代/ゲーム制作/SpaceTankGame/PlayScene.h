#pragma once

#include "Engine/global.h"
#include "Engine/DirectX/Text.h"

class Player;

class PlayScene : public IGameObject 
{
	//分の値指定用　10からここで指定した数を引いた時間が設定される
	//9と書いた場合　10 - 9 = 1 なので表示時間は一分から開始
	const unsigned short MIN_COUNT = 8;
	const unsigned short FRAME_MIN = 0;

	const float DELIMITR = 47.6f;					//HPゲージ一本分の数値
	const unsigned short INITIAL_HP = 10;			//初期HP
	const unsigned short NUM_HUNDREDS = 540;		//残りタイムの100の位
	const unsigned short NUM_TENSPLACE = 600;		//残りタイムの10の位
	const unsigned short NUM_FIRSTPLACE = 660;		//残りタイムの1の位

private:

	int hSound_;		//サウンド番号

	//時間の処理
	Text* pText_;		//テキスト
	Text* pText2_;		//テキスト
	Text* pText3_;		//テキスト

	int minCnt_;		//分カウント配列の添字用

	char *sec_[11];
	char *min_[11];

	int cnt1_;			//１のくらいのカウント用
	int cnt10_;			//１０のくらいのカウント用
	int frame_;			//フレームカウント用変数
	int hPict_;			//画像番号
	int hImage_[5];		//画像番号

	int  lifeBar_;		//ライフバーの位置の管理

	Player* pPlayer_;

	int preHp_;			//一つ前のHPを覚えておく
	int lifePosition_;	//ライフの位置をずらす

public:

	Player* GetPlayer()
	{
		//ポインタ
		return pPlayer_;
	}


	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	PlayScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//時間を設定する
	void InitTime();

	//更新
	void Update() override;

	//開発者用コマンド
	void DeveloperCommands();

	//時間切れの時の処理
	void TimeOut();

	//HPの位置を管理する
	void HpManagement();

	//時間を表示する
	void TimeCount();

	//描画
	void Draw() override;

	//開放
	void Release() override;
};
