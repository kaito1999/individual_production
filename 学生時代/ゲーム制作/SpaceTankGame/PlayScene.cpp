#include "PlayScene.h"
#include "Engine/global.h"
#include "Player.h"
#include "Stage.h"
#include "Enemy.h"
#include "Water.h"
#include "Item.h"
#include "BackGround.h"
#include "Engine/ResouceManager/Image.h"
#include "Engine/ResouceManager/Audio.h"
#include "Water.h"


PlayScene::PlayScene(IGameObject * parent)
	: IGameObject(parent, "PlayScene")
	, pText_(nullptr), pText2_(nullptr), pText3_(nullptr)
	, cnt1_(0), cnt10_(0), minCnt_(MIN_COUNT)//,ENEMY_NUM(5)
	, frame_(FRAME_MIN)
	, hPict_(-1), lifeBar_(0), lifePosition_(0), preHp_(INITIAL_HP)
	, hSound_(-1)
{
}

//初期化
void PlayScene::Initialize()
{
		//テキストを作成
		//_pText = new Text("ＭＳ ゴシック", 32);

		//ステージクラス
		CreateGameObject<Stage>(this);
		pPlayer_ = CreateGameObject<Player>(this);

		//敵を増やす可能性を考慮しておく
		//for (int i = 0; i < ENEMY_NUM; i++)
		//{

		//敵クラス
		CreateGameObject<Enemy>(this);

		//}

		//アイテムクラス
		CreateGameObject<Item>(this);

		//バッググラウンドクラスクラス
		CreateGameObject<BackGround>(this);

		//ウォータークラス
		CreateGameObject<Water>(this);

		//画像データのロード
		hPict_ = Image::Load("data/Picture/HpBlackFrame.png");
		assert(hPict_ >= 0);

		std::string fileName[] =
		{
			"data/Picture/HP_black.png",
			"data/Picture/HP_red_gauge.png",
			"data/Picture/HP_Yellow_gauge.png",
			"data/Picture/HP_green_gauge.png",
			"data/Picture/HPframe2.png",
		};
		//登録した画像を読み込み
		for (int i = 0; i < 5; i++)
		{
			hImage_[i] = Image::Load(fileName[i]);
		}


		//残りHPを割って色を判断する
		lifeBar_ = (pPlayer_->hpCom_) / 3 + 1;

		
		//制限時間用
		//テキストを作成
		pText_ = new Text("メイリオ", 100);
		pText2_ = new Text("メイリオ", 100);
		pText3_ = new Text("メイリオ", 100);
		

		//色変更
		pText_->SetColor (255, 100,   0, 255);
		pText2_->SetColor(255, 100,   0, 255);
		pText3_->SetColor(255, 100,   0, 255);
		

		//時間の文字を出す
		InitTime();

		//サウンドデータのロード
		hSound_ = Audio::Load("data/Sound/Battle.wav");
		assert(hSound_ >= 0);

		Audio::Play(hSound_);
}

//時間を設定する
void PlayScene::InitTime()
{
	sec_[0] = "0";
	sec_[1] = "9";
	sec_[2] = "8";
	sec_[3] = "7";
	sec_[4] = "6";
	sec_[5] = "5";
	sec_[6] = "4";
	sec_[7] = "3";
	sec_[8] = "2";
	sec_[9] = "1";
	sec_[10] = "0";

	min_[0] = "0";
	min_[1] = "9";
	min_[2] = "8";
	min_[3] = "7";
	min_[4] = "6";
	min_[5] = "5";
	min_[6] = "4";
	min_[7] = "3";
	min_[8] = "2";
	min_[9] = "1";
	min_[10] = "0";
}

//更新
void PlayScene::Update()
{
	//開発者用コマンド
	DeveloperCommands();

	//時間切れの時の処理
	TimeOut();

	//時間を表示する
	TimeCount();

	//HPの位置を管理する
	HpManagement();

}

//開発者用コマンド
void PlayScene::DeveloperCommands()
{
	//QとMとP同時に押したら開発者用コマンドシーンへ
	if (Input::IsKey(DIK_Q) && Input::IsKey(DIK_M) && Input::IsKey(DIK_P))
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_DEVELOPERCOMMANDS);
		Audio::Stop(hSound_);
	}
}

//時間切れの時の処理
void PlayScene::TimeOut()
{
	//カウントダウンが0になったらタイトルシーンへ移行
	if (sec_[cnt1_] == "0" && sec_[cnt10_] == "0" && sec_[minCnt_] == "0")
	{
		//ゲームオーバーシーンへ
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_GAMEOVER);

		Audio::Stop(hSound_);

	}
}

//HPの位置を管理する
void PlayScene::HpManagement()
{
	//これでplayerの変数を持ってきている
	lifeBar_ = pPlayer_->hpCom_ / 4 + 1;

	//プレイヤーのHPとこのクラスの確認用のHPを比較して減ったらライフバーをずらす
	if (pPlayer_->hpCom_ < preHp_)
	{
		lifePosition_ -= DELIMITR;

		preHp_ = (pPlayer_->hpCom_);
	}
}

//時間を表示する
void PlayScene::TimeCount()
{
	//フレームのカウントが60でちょうど1秒
	frame_++;

	if (frame_ == 60)
	{
		frame_ = 0;
		//**が９より小さければ

		if (minCnt_ <= 10)
		{

			//最初3:00から2:59にする
			if (cnt10_ == 0 && cnt1_ == 0)
			{
				cnt10_ = 1;
				cnt1_ = 0;
				minCnt_++;
			}

			//cnt10は10の位のカウント
			if (cnt10_ <= 10)
			{
				if (cnt10_ == 10 && cnt1_ == 10)
				{
					cnt10_ = 1;
					cnt1_  = 1;
					minCnt_++;
				}

				//1の位のカウント
				else if (cnt1_ < 10)
				{
					cnt1_++;
				}

				else if (cnt1_ == 10)
				{
					cnt1_ = 1;
					cnt10_++;
				}

			}
		
		}
	}
}

void PlayScene::Draw()
{
	//第一引数は横幅、第二引数は縦幅位置情報
	pText_->Draw(NUM_HUNDREDS, 10, min_[minCnt_] ); //自動で2:59にする
	pText_->Draw(NUM_TENSPLACE, 10, min_[cnt10_]); //自動で2:59にする
	pText_->Draw(NUM_FIRSTPLACE, 10, min_[cnt1_]); //自動で2:59にする
	


	D3DXMATRIX m;
	D3DXMatrixTranslation(&m, lifePosition_, 27, 0);

	D3DXMATRIX m2;
	D3DXMatrixTranslation(&m2, 0, 27, 0);

	//HPの下に表示する
	{
		Image::SetMatrix(hPict_, _worldMatrix*m2);
		Image::Draw(hPict_);
	}
	//HPをずらす
	{
		Image::SetMatrix(hImage_[lifeBar_], _localMatrix * m);
		Image::Draw(hImage_[lifeBar_]);
	}
	//HPの枠
	{
		Image::SetMatrix(hImage_[4], _localMatrix * m2);
		Image::Draw(hImage_[4]);
	}

	
}

void PlayScene::Release()
{
	delete pText_;
	delete pText2_;
	delete pText3_;

	//シーンが終わったら音楽を止めたい
	Audio::Stop(hSound_);
	
}
