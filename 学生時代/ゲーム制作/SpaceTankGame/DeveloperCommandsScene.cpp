#include "DeveloperCommandsScene.h"
#include "Engine/ResouceManager/Image.h"


//コンストラクタ
DeveloperCommandsScene::DeveloperCommandsScene(IGameObject * parent)
	: IGameObject(parent, "DeveloperCommandsScene")
{
}

//初期化
void DeveloperCommandsScene::Initialize()
{
	//画像の管理
	std::string fileName[] =
	{
		"data/Picture/DeveloperCommands.png",
	};
	//登録した画像を読み込み
	for (int i = 0; i < 1; i++)
	{
		hImage_[i] = Image::Load(fileName[i]);
	}
}

//更新
void DeveloperCommandsScene::Update()
{
	//シーン切り替え
	SceneSwitching();
}

//シーン切り替え
void DeveloperCommandsScene::SceneSwitching()
{
	//1を押したらタイトルシーンへ
	if (Input::IsKey(DIK_1))
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_TITLE);
	}

	//2を押したら説明画面シーンへ
	if (Input::IsKey(DIK_2))
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_DESCRIPTION);
	}

	//3を押したらプレイシーンへ
	if (Input::IsKey(DIK_3))
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_PLAY);
	}

	//4を押したらクリアシーンへ
	if (Input::IsKey(DIK_4))
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_CLEAR);
	}

	//5を押したらゲームオーバーシーンへ
	if (Input::IsKey(DIK_5))
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_GAMEOVER);
	}

	//6を押したらリザルトシーンへ
	if (Input::IsKey(DIK_6))
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_RESULT);
	}
}

//描画
void DeveloperCommandsScene::Draw()
{
	Image::SetMatrix(hImage_[0], _localMatrix);
	Image::Draw(hImage_[0]);
}

//開放
void DeveloperCommandsScene::Release()
{
}