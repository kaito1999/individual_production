#pragma once
#include "Engine/GameObject/GameObject.h"



//アイテムを管理するクラス
class Item : public IGameObject
{
	//説明画面
	const unsigned short ADJUSTMENT = 3;	 //位置調整用
	const unsigned short POSITIONED = 10;	 //位置調整用


	int hModel_[1];							//画像を配列で管理
	int frame_;								//アップデートで回してフレームとして使う
	
public:

	

	//コンストラクタ
	Item(IGameObject* parent);

	//デストラクタ
	~Item();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//ランダム生成
	void RandomGeneration();

	//描画
	void Draw() override;

	//開放
	void Release() override;


	//何かに当たった
	//引数：pTarget　当たった相手
	void OnCollision(IGameObject * pTarget) override;

};