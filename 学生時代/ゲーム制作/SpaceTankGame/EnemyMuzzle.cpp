#include "EnemyMuzzle.h"
#include "Engine/gameObject/Camera.h"
#include "Engine/ResouceManager/Model.h"
#include "EnemyBullet.h"

//コンストラクタ
EnemyMuzzle::EnemyMuzzle(IGameObject * parent)
	:IGameObject(parent, "EnemyMuzzle"),hModel_(-1)
	,Frame(0), AIMove(0)
{
}

//デストラクタ
EnemyMuzzle::~EnemyMuzzle()
{
}

//初期化
void EnemyMuzzle::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("data/Model/TankHead.fbx");
	assert(hModel_ >= 0);

	//サイズ
	//親のEnemyクラスで、小さくしてしまったので、サイズを元に戻した
	_scale *= 5;

	//砲台の位置調整
	position_.y += POSITIONED;
	position_.z += POSITIONED;
	
}

//更新
void EnemyMuzzle::Update()
{
	//ランダムで球を打つか打たないか決める
	BulletMove();

}
//ランダムで球を打つか打たないか決める
void EnemyMuzzle::BulletMove()
{
	//アップデートをぐるぐる回してフレームを進ませる
	Frame++;
	//ランダムで球を打つか打たないか決める
	if (Frame == 25)
	{
		AIMove = rand() % 4;

		switch (AIMove)
		{
		case 0: //何もしない
			break;
		case 1:	Shot(); //球を打つ
			break;
		case 2:		Shot(); //球を打つ
			break;
		case 3:		Shot(); //球を打つ
			break;
		}
		Frame = 0;
	}
}

//弾を撃つ
void EnemyMuzzle::Shot()
{
		
	IGameObject* playScene = FindObject("PlayScene");
	EnemyBullet* pEnemyBullet = CreateGameObject<EnemyBullet>(FindObject("PlayScene"));


	D3DXVECTOR3 shotPos = Model::GetBonePosition(hModel_, "ShotPoint");    //発射口
	D3DXVECTOR3 cannonRoot = Model::GetBonePosition(hModel_, "CannonRoot");//発生元
	pEnemyBullet->Shot(shotPos, (shotPos - cannonRoot));
}
//描画
void EnemyMuzzle::Draw()
{
	Model::SetMatrix(hModel_, _worldMatrix);
	Model::Draw(hModel_);
}

//開放
void EnemyMuzzle::Release()
{
}