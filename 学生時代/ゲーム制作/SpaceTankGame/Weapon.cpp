#include "Weapon.h"

//コンストラクタ
Weapon::Weapon(IGameObject * parent)
	:IGameObject(parent, "Weapon")
{
}

//デストラクタ
Weapon::~Weapon()
{
}

//初期化
void Weapon::Initialize()
{
}

//更新
void Weapon::Update()
{
}

//描画
void Weapon::Draw()
{
}

//開放
void Weapon::Release()
{
}