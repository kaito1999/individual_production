#include<time.h>
#include "DescriptionScene.h"
#include "Engine/ResouceManager/Image.h"
#include "Engine/ResouceManager/Audio.h"

//コンストラクタ
DescriptionScene::DescriptionScene(IGameObject * parent)
	: IGameObject(parent, "DescriptionScene")
	,descriptionCount_(0), sideMove_(0), heightMove_(INITIAL_POS)
	, clickFlag_(TRUE), timeFlag_(false), time_(0), timeBGM_(0)
{
}

//初期化
void DescriptionScene::Initialize()
{
	std::string fileName[] =
	{
		"data/picture/Description.png",
		"data/picture/Load.png",
		"data/Picture/Galaxy.png",
		"data/Picture/PushSpaceNormal.png",
		"data/Picture/PushSpacePress.png",
	};

	//登録した画像を読み込み
	for (int i = 0; i < 5; i++)
	{
		hImage_[i] = Image::Load(fileName[i]);
	}

	std::string SoundfileName[] =
	{
		"data/Sound/DESCRIPTION.wav",
		"data/Sound/SelectSE.wav",
	};
	//登録した音源を読み込み
	for (int i = 0; i < 2; i++)
	{
		hSound_[i] = Audio::Load(SoundfileName[i]);
	}

	//BGM
	Audio::Play(hSound_[0]);
	
}

//更新
void DescriptionScene::Update()
{
	//開発者用コマンド
	DeveloperCommands();

	//曲をループさせる
	MusicLoop();

	//Timeをリセットする
	TimeClockReset();
	
	//スペースを押している画像の切り替えに使う
	PushSwitching();

	//背景動かす処理
	BackGroundMove();

	//次のシーンへ
	ChangeScene();
}

//開発者用コマンド
void DescriptionScene::DeveloperCommands()
{
	//QとMとP同時に押したら開発者用コマンドシーンへ
	if (Input::IsKey(DIK_Q) && Input::IsKey(DIK_M) && Input::IsKey(DIK_P))
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_DEVELOPERCOMMANDS);

	}
}

//曲をループさせる
void DescriptionScene::MusicLoop()
{
	//曲をループさせる
	if (clock() - timeBGM_ >= ONE_LOOP)
	{
		//一度止めてから
		Audio::Stop(hSound_[0]);

		//ループさせる
		Audio::Play(hSound_[0]);

		timeBGM_ = clock();
	}
}

//次のシーンへ
void DescriptionScene::ChangeScene()
{
	//最初の画像を出しているときかつSPACEが押されたらプレイシーンに移る
	if (descriptionCount_ == 0 && Input::IsKeyDown(DIK_SPACE))
	{
		//出す画像を一つ先のものにする
		descriptionCount_ = 1;

		//効果音
		Audio::Play(hSound_[1]);

		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_PLAY);
	}
}

//Spaceの描画のための
void DescriptionScene::PushSwitching()
{
	//2秒くらい経った時
	if (clock() - time_ >= TWO_SECONDS)
	{
		clickFlag_ = FALSE;
	}
	//3秒くらい経った時
	if (clock() - time_ >= THREE_SECONDS)
	{
		clickFlag_ = TRUE;
	}
	//3秒くらい経った時
	if (clock() - time_ > THREE_SECONDS)
	{
		//タイムをリセット
		time_ = clock();
	}
}

//Timeをリセットする
void DescriptionScene::TimeClockReset()
{
	//リセットされてないなら
	if (!timeFlag_)
	{
		//タイムをリセット
		time_ = clock();
		//タイムをリセット
		timeBGM_ = clock();

		//フラグをオンで二回目の初期化をしない
		timeFlag_ = true;
	}
}

//背景動かす処理
void DescriptionScene::BackGroundMove()
{
	//ここの二つの変数で背景を動かしている
	{
		sideMove_--;
		heightMove_++;
	}
	//背景の大きさぎりぎりまで来たら初期位置に戻す
	if (heightMove_ > MAX_VAL)
	{
		sideMove_ = RESET;
		heightMove_ = INITIAL_POS;
	}
}

//描画
void DescriptionScene::Draw()
{

	//動かしているほうの背景
	D3DXMATRIX m;
	D3DXMatrixTranslation(&m, sideMove_, heightMove_, 0);
	{
		//背景表示
		Image::SetMatrix(hImage_[2], _localMatrix*m);
		Image::Draw(hImage_[2]);
	}

	//説明画面の
	Image::SetMatrix(hImage_[descriptionCount_], _localMatrix  );
	Image::Draw(hImage_[descriptionCount_]);

	//PushSpaceを表示する部分
	{
		//画像の位置
		D3DXMATRIX m2;
		D3DXMatrixTranslation(&m2, SPACE_SIDE, SPACE_HEIGHT, 0);
		//最初の画像が表示されているとき
		if (descriptionCount_ == 0)
		{
			//フラグの切り替えによって画像も切り替わるようにした
			if (clickFlag_ == TRUE)
			{
				//spaceボタンの押されていない画像
				Image::SetMatrix(hImage_[3], _localMatrix*m2);
				Image::Draw(hImage_[3]);
			}
			else
			{
				//spaceボタンの押されている画像
				Image::SetMatrix(hImage_[4], _localMatrix*m2);
				Image::Draw(hImage_[4]);
			}
		}
	}
}

//開放
void DescriptionScene::Release()
{
	Audio::Stop(hSound_[0]);
}