#include "outerwall.h"
#include "Engine/ResouceManager/Model.h"
//コンストラクタ
outerwall::outerwall(IGameObject * parent)
	:IGameObject(parent, "outerwall"), _hModel(-1)
{
}

//デストラクタ
outerwall::~outerwall()
{
}

//初期化
void outerwall::Initialize()
{
	//モデルデータのロード
	_hModel = Model::Load("data/Model/OuterWall.fbx");
	assert(_hModel >= 0);

	_scale = D3DXVECTOR3(-1, -1, -1);
	
}

//更新
void outerwall::Update()
{
}

//描画
void outerwall::Draw()
{
	Model::SetMatrix(_hModel, _worldMatrix);
	Model::Draw(_hModel);
}

//開放
void outerwall::Release()
{
}