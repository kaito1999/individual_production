#pragma once
#include "Engine/GameObject/GameObject.h"

//◆◆◆を管理するクラス
class outerwall : public IGameObject
{
	int _hModel;    //モデル番号
public:
	//コンストラクタ
	outerwall(IGameObject* parent);

	//デストラクタ
	~outerwall();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};