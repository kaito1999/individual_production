#include "Item.h"
#include "Engine/ResouceManager/Model.h"
#include "PlayScene.h"
#include "Player.h"


//コンストラクタ
Item::Item(IGameObject * parent)
	:IGameObject(parent, "Item"), frame_(0)
{
}

//デストラクタ
Item::~Item()
{
}

//初期化
void Item::Initialize()
{
	//画像の管理
	std::string fileName[] =
	{
		"data/Model/PowerUp.fbx",
	};

	//登録したモデルを読み込み
	for (int i = 0; i < 1; i++)
	{
		hModel_[i] = Model::Load(fileName[i]);
	}
	
	//当たり判定追加
	SphereCollider* collision = new SphereCollider(D3DXVECTOR3(0, 0, 0), 0);
	AddCollider(collision);

}

//更新
void Item::Update()
{
	//アップデートで回してフレームとして使う
	frame_++;

	//ランダム生成
	RandomGeneration();
}

//ランダム生成
void Item::RandomGeneration()
{
	if (frame_ % 300 == 0)
	{
		//位置
		position_.x = (rand() % POSITIONED) + ADJUSTMENT;
		position_.z = (rand() % POSITIONED) + ADJUSTMENT;
	}
}

//描画
void Item::Draw()
{
	Model::SetMatrix(hModel_[0], _localMatrix);
	Model::Draw(hModel_[0]);
}

//開放
void Item::Release()
{
}

//何かに当たった
void Item::OnCollision(IGameObject * pTarget)
{
	//プレイヤーに当たった
	if (pTarget->GetObjectName() == "Player")
	{
		((PlayScene*)GetParent())->GetPlayer()->PowerFlag(TRUE);
		KillMe();
	}
}