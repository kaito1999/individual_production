#pragma once
#include "Engine/GameObject/GameObject.h"

//背景を管理するクラス
class BackGround : public IGameObject
{
	
	int hModel_;	//モデル番号

public:
	//コンストラクタ
	BackGround(IGameObject* parent);

	//デストラクタ
	~BackGround();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//常時回転させたい
	void KeepSpinning();

	//描画
	void Draw() override;

	//開放
	void Release() override;
};