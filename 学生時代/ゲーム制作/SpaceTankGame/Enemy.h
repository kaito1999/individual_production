#pragma once
#include "Engine/GameObject/GameObject.h"
#include "Stage.h"
#include "PlayScene.h"

#include "P3DEngine.h"

#if _DEBUG
#pragma comment(lib, "P3DEngineD.lib")
#else
#pragma comment(lib, "P3DEngine.lib")
#endif


//敵を管理するクラス
class Enemy : public IGameObject
{
	//定数宣言
	const unsigned short ONEHUNDREDEIGHTY_SECONDS = 180;	//体力180
	const unsigned short TWOHUNDREDFORTY_SECONDS = 240;		//体力240


	int hModel_;				//モデル番号
	int hSound_[1];				//サウンド番号
	P3DEngine* pP3D_;			//P3DEngine型のポインタを用意

private:
	
	int hp_;					//キャラクターのHPの処理
	int aiMove_;				//AIでどう動くかを決める
	int moveCount_;				//とりあえずカウントで時間を図る
	int hEnemyBulletModel_;		//弾のモデル番号
	int hModel2_[3];			//画像を配列で管理する
	int lifePict_;				//HPの割合でHPの色を変化するようにする
	int frame_;					//updataでグルグル回してフレームとして使う
	bool changeSceneFlag_;		//シーンを切り替えるために使う
	
	Stage* pStage_;
	

public:
	Enemy(IGameObject* parent);
	~Enemy();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//次のシーンへ
	void ChangeScene();

	//体力に応じた行動
	void ActForStrength();

	//体力に応じた体力バーの変化
	void LifeBarColor();

	//敵の動き
	void Move();

	//敵の打つ弾
	void Shot();

	//描画
	void Draw() override;

	//開放
	void Release() override;
	//壁を出現させる
	void Wall(D3DXVECTOR3 &prePos);
	
	//何かに当たった
	//引数：pTarget　当たった相手
	void OnCollision(IGameObject * pTarget) override;

};