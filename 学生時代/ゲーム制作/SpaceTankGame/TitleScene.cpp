#include<time.h>
#include "TitleScene.h"
#include "Engine/ResouceManager/Image.h"
#include "Engine/ResouceManager/Audio.h"

TitleScene::TitleScene(IGameObject * parent)
	: IGameObject(parent, "BootScene")
	, sideMove_(0), heightMove_(INITIAL_POS), clickFlag_(TRUE)
	, timeFlag_(false), time_(0), timeBGM_(0)
{
}

//初期化
void TitleScene::Initialize()
{
	//画像の管理
	std::string fileName[] =
	{
		"data/Picture/Galaxy.png",
		"data/Picture/title.png",
		"data/Picture/PushSpaceNormal.png",
		"data/Picture/PushSpacePress.png",
	};
	//登録した画像を読み込み
	for (int i = 0; i < 4; i++)
	{
		hImage_[i] = Image::Load(fileName[i]);
	}

	//音源の読み込み
	std::string SoundfileName[] =
	{
		"data/Sound/TITLE.wav",
		"data/Sound/SelectSE.wav",
	};
	//登録した音源を読み込み
	for (int i = 0; i < 2; i++)
	{
		hSound_[i] = Audio::Load(SoundfileName[i]);
	}

	//BGM
	Audio::Play(hSound_[0]);
}

//更新
void TitleScene::Update()
{
	//開発者用コマンド
	DeveloperCommands();

	//曲をループさせる
	MusicLoop();

	//Timeをリセットする
	TimeClockReset();

	//スペースを押している画像の切り替えに使う
	PushSwitching();

	//画像を動かす処理
	BackGroundMove();

	//次のシーンへ
	ChangeScene();

}

//開発者用コマンド
void TitleScene::DeveloperCommands()
{
	//QとMとP同時に押したら開発者用コマンドシーンへ
	if (Input::IsKey(DIK_Q) && Input::IsKey(DIK_M) && Input::IsKey(DIK_P))
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_DEVELOPERCOMMANDS);

	}
}

//次のシーンへ
void TitleScene::ChangeScene()
{
	//スペース押したら説明シーンへ
	if (Input::IsKeyDown(DIK_SPACE))
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_DESCRIPTION);

		//効果音
		Audio::Play(hSound_[1]);
	}
}

//曲をループさせる
void TitleScene::MusicLoop()
{
	//曲をループさせる
	if (clock() - timeBGM_ >= ONE_LOOP)
	{
		//一度止めてから
		Audio::Stop(hSound_[0]);

		//ループさせる
		Audio::Play(hSound_[0]);

		timeBGM_ = clock();
	}
}

//スペースを押している画像の切り替えに使う
void TitleScene::PushSwitching()
{
	//2秒くらい経った時
	if (clock() - time_ >= TWO_SECONDS)
	{
		clickFlag_ = FALSE;
	}
	//3秒くらい経った時
	if (clock() - time_ >= THREE_SECONDS)
	{
		clickFlag_ = TRUE;
	}
	//3秒くらい経った時
	if (clock() - time_ > THREE_SECONDS)
	{
		//タイムをリセット
		time_ = clock();
	}
}

//Timeをリセットする
void TitleScene::TimeClockReset()
{
	//リセットされてないなら
	if (!timeFlag_)
	{
		//タイムをリセット
		time_ = clock();
		//タイムをリセット
		timeBGM_ = clock();

		//フラグをオンで二回目の初期化をしない
		timeFlag_ = true;
	}
}

//画像を動かす処理
void TitleScene::BackGroundMove()
{
	//背景の位置を動かす
	{
		sideMove_--;
		heightMove_++;
	}
	//背景の大きさぎりぎりまで来たら初期位置に戻す
	if (heightMove_ > MAX_VAL)
	{
		sideMove_ = RESET; //位置をリセットする
		heightMove_ = INITIAL_POS;
	}
}

//描画
void TitleScene::Draw()
{
	//背景の宇宙を動かしている
	{
		D3DXMATRIX m;
		D3DXMatrixTranslation(&m, sideMove_, heightMove_, 0);

		Image::SetMatrix(hImage_[0], _localMatrix*m);
		Image::Draw(hImage_[0]);
	}

	//タイトル部分の表示
	{
		//画像の位置
		D3DXMATRIX m2;
		D3DXMatrixTranslation(&m2, TITLE_POS_SIDE, TITLE_POS_HEIGHT, 0);
		Image::SetMatrix(hImage_[1], _localMatrix*m2);
		Image::Draw(hImage_[1]);
	}

	//PushSpaceを表示する部分
	{
		//画像の位置
		D3DXMATRIX m3;
		D3DXMatrixTranslation(&m3, SPACE_SIDE, SPACE_HEIGHT, 0);

		//フラグの切り替えによって画像も切り替わるようにした
		if (clickFlag_ == TRUE)
		{
			Image::SetMatrix(hImage_[2], _localMatrix*m3);
			Image::Draw(hImage_[2]);
		}
		else
		{
			Image::SetMatrix(hImage_[3], _localMatrix*m3);
			Image::Draw(hImage_[3]);
		}
		
	}
}

//開放
void TitleScene::Release()
{
	//音楽を止める
	Audio::Stop(hSound_[0]);
}
