#pragma once

enum SCENE_ID 
{
	//出したいSceneを全部書くを
	SCENE_ID_TITLE = 0,
	SCENE_ID_DESCRIPTION,
	SCENE_ID_PLAY,
	SCENE_ID_CLEAR,
	SCENE_ID_GAMEOVER,
	SCENE_ID_RESULT,
	SCENE_ID_DEVELOPERCOMMANDS
};