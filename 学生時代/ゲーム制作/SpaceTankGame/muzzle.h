#pragma once
#include "Engine/GameObject/GameObject.h"
#include "Engine/DirectX/Text.h"



//マゼルを管理するクラス
class muzzle : public IGameObject
{
	//定数宣言
	const unsigned short FRAME_MAX = 60;		//60秒
	const unsigned short FRAME_MIN = 0;			//0秒
	const unsigned short REMAINING_BULLET = 5;	//残り残弾
	const unsigned short SET_GAUGE_BAR = 1040;	//チャージバーの初期位置
	const unsigned short OUTSIDE_SCREEN = 1280;	//スクリーンの限界


	int hModel_;				//モデル番号
	int coolTime_;				//リロードの時のタイマーとして使う
	int stock_;					//残弾の表示
	int hImage_[10];			//画面に表示するうえで必要そうなものを、配列で管理する
	int chargeTime_;			//弾を強化するためのカウント
	int hPImage_[2];			//強化関連の画像管理	
	bool powerUpFlag_;			//強化可能かどうかを確認する
	int powerUpCount_;			//パワーアップ時間の残量のゲージバーを動かすための変数
	int hSound_;				//サウンド番号



public:
	//コンストラクタ
	muzzle(IGameObject* parent);

	//デストラクタ
	~muzzle();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//強化が可能かどうかを表示する
	void PowerUp();

	//弾のリロード
	void Reload();

	//弾を撃つ
	void Shot();

	//強化された弾を打つ
	void ChargeGun();

	//普通の弾を打つ
	void Gun();

	//描画
	void Draw() override;

	//開放
	void Release() override;
};
