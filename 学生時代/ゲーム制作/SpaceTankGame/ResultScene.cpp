#include<time.h>
#include "ResultScene.h"
#include "Engine/ResouceManager/Image.h"
#include "Engine/ResouceManager/Audio.h"

//コンストラクタ
ResultScene::ResultScene(IGameObject * parent)
	: IGameObject(parent, "ResultScene")
	, continueFlag_(true), sideMove_(0), heightMove_(INITIAL_POS)
	, timeFlag_(false),timeBGM_(0)
{
}

//初期化
void ResultScene::Initialize()
{
	std::string fileName[] =
	{
		"data/Picture/result.png",
		"data/Picture/Continue.png",
		"data/Picture/Cancel.png",
		"data/Picture/Select.png",
		"data/Picture/Galaxy.png",
		"data/Picture/ContinueDark.png",
		"data/Picture/CancelDark.png",
	};

	//登録した画像を読み込み
	for (int i = 0; i < 7; i++)
	{
		hImage_[i] = Image::Load(fileName[i]);
	}


	std::string SoundfileName[] =
	{
		"data/Sound/SelectSE.wav",
		"data/Sound/SelectChangeSE.wav",
		"data/Sound/RESULT.wav",
	};
	//登録した音源を読み込み
	for (int i = 0; i < 3; i++)
	{
		hSound_[i] = Audio::Load(SoundfileName[i]);
	}

	//BGM
	Audio::Play(hSound_[2]);

}

//更新
void ResultScene::Update()
{
	//開発者用コマンド
	DeveloperCommands();

	//開発者用コマンド
	MusicLoop();

	//Timeをリセットする
	TimeClockReset();

	//背景を動かす処理
	BackGroundMove();

	//次のシーンへ
	ChangeScene();

}

//開発者用コマンド
void ResultScene::DeveloperCommands()
{
	//QとMとP同時に押したら開発者用コマンドシーンへ
	if (Input::IsKey(DIK_Q) && Input::IsKey(DIK_M) && Input::IsKey(DIK_P))
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_DEVELOPERCOMMANDS);
	}
}

//次のシーンへ
void ResultScene::ChangeScene()
{
	//spaceかenterを押したときカーソルがつづけるならプレイシーンへ
	if (Input::IsKeyDown(DIK_RETURN) || Input::IsKeyDown(DIK_SPACE) && continueFlag_ == true)
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_PLAY);

		//効果音
		Audio::Play(hSound_[0]);
	}
	//spaceかenterを押したときカーソルがタイトルへならタイトルへ
	if (Input::IsKeyDown(DIK_RETURN) || Input::IsKeyDown(DIK_SPACE) && continueFlag_ == false)
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_TITLE);

		//効果音
		Audio::Play(hSound_[0]);
	}

	//→キー押したらタイトルへにカーソルを合わせる
	if (Input::IsKeyDown(DIK_RIGHT) && continueFlag_ == true)
	{
		continueFlag_ = false;

		//効果音
		Audio::Play(hSound_[1]);
	}
	//←キー押したらつづけるにカーソルを合わせる
	if (Input::IsKeyDown(DIK_LEFT) && continueFlag_ == false)
	{
		continueFlag_ = true;
		//効果音
		Audio::Play(hSound_[1]);
	}
}

//曲のループさせる
void ResultScene::MusicLoop()
{
	//曲をループさせる
	if (clock() - timeBGM_ >= ONE_LOOP)
	{
		//一度止めてから
		Audio::Stop(hSound_[2]);

		//ループさせる
		Audio::Play(hSound_[2]);

		timeBGM_ = clock();
	}
}

//Timeをリセットする
void ResultScene::TimeClockReset()
{
	//リセットされてないなら
	if (!timeFlag_)
	{

		//タイムをリセット
		timeBGM_ = clock();

		//フラグをオンで二回目の初期化をしない
		timeFlag_ = true;
	}
}

//画像を動かす処理
void ResultScene::BackGroundMove()
{
	//背景の位置を動かす
	{
		sideMove_--;
		heightMove_++;
	}

	//背景の大きさぎりぎりまで来たら初期位置に戻す
	if (heightMove_ > MAX_VAL)
	{
		sideMove_ = RESET;
		heightMove_ = INITIAL_POS;
	}
}

//描画
void ResultScene::Draw()
{
	D3DXMATRIX m;
	D3DXMatrixTranslation(&m, sideMove_, heightMove_, 0);
	//背景表示
	Image::SetMatrix(hImage_[4], _localMatrix*m);
	Image::Draw(hImage_[4]);

	//リザルト表示
	D3DXMATRIX mR;
	D3DXMatrixTranslation(&mR, 0, RESULT_HEIGHT, 0);
	Image::SetMatrix(hImage_[0], _localMatrix *mR);
	Image::Draw(hImage_[0]);
	
	//つづけるの表示
	{

		if (continueFlag_ == true)
		{
			//つづけるの画像
			D3DXMATRIX mToBe;
			D3DXMatrixTranslation(&mToBe, CONTINUE_SELECT_SIDE, CONTINUE_SELECT_HEIGHT, 0);
			Image::SetMatrix(hImage_[1], _localMatrix*mToBe);
			Image::Draw(hImage_[1]);
		}
		else
		{
			//つづけるの画像
			D3DXMATRIX mToBe;
			D3DXMatrixTranslation(&mToBe, CONTINUE_SELECT_SIDE, CONTINUE_SELECT_HEIGHT, 0);
			Image::SetMatrix(hImage_[5], _localMatrix*mToBe);
			Image::Draw(hImage_[5]);
		}
		
	}

	//タイトルへの表示
	{
		if (continueFlag_ == false)
		{
			//タイトルへの画像
			D3DXMATRIX m2;
			D3DXMatrixTranslation(&m2, TITLE_SELECT_SIDE, TITLE_SELECT_HEIGHT, 0);
			Image::SetMatrix(hImage_[2], _localMatrix*m2);
			Image::Draw(hImage_[2]);
		}
		else
		{
			//タイトルへの画像
			D3DXMATRIX m2;
			D3DXMatrixTranslation(&m2, TITLE_SELECT_SIDE, TITLE_SELECT_HEIGHT, 0);
			Image::SetMatrix(hImage_[6], _localMatrix*m2);
			Image::Draw(hImage_[6]);
		}
	}


	//ここから下はselect
	{
		if (continueFlag_ == true)
		{
			//位置調整は必要かも
			D3DXMATRIX m3;
			D3DXMatrixTranslation(&m3, SELECT_LEFT, SELECT_HEIGHT, 0);
			Image::SetMatrix(hImage_[3], _localMatrix*m3);
			Image::Draw(hImage_[3]);
		}
		else
		{
			//位置調整は必要かも
			D3DXMATRIX m4;
			D3DXMatrixTranslation(&m4, SELECT_RIGHT, SELECT_HEIGHT, 0);
			Image::SetMatrix(hImage_[3], _localMatrix*m4);
			Image::Draw(hImage_[3]);
		}
	}

	
}

//開放
void ResultScene::Release()
{
	Audio::Stop(hSound_[2]);
}


