#pragma once
#include "Engine/GameObject/GameObject.h"

//武器を管理するクラス
class Weapon : public IGameObject
{

public:
	//コンストラクタ
	Weapon(IGameObject* parent);

	//デストラクタ
	~Weapon();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//描画
	void Draw() override;

	//開放
	void Release() override;
};