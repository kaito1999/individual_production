#pragma once
#include "Engine/global.h"


//説明シーンを管理するクラス
class DescriptionScene : public IGameObject
{

	//定数宣言
	const unsigned int ONE_LOOP = 133000;			//曲一回分2分13秒
	const unsigned short TWO_SECONDS = 2000;		//2秒
	const unsigned short THREE_SECONDS = 3000;		//３秒
	const short INITIAL_POS = -1200;				//初期位置
	const unsigned short MAX_VAL = 0;				//最大値
	const unsigned short RESET = 0;					//リセット
	const unsigned short SPACE_SIDE = 400;			//spaceボタンの横の位置
	const unsigned short SPACE_HEIGHT = 580;		//spaceボタンの高さの位置



	int hSound_[2];				//サウンド番号
	int hImage_[5];				//画面に表示するうえで必要なものを、配列で管理する
	int descriptionCount_;		//画像が配列で2つのみ管理されているのでカウントが1以上にならないようにする
	int sideMove_;				//横の移動量
	int heightMove_;			//縦の移動量
	bool clickFlag_;			//spaceボタンの押される画像を切り替えるフラグ
	bool timeFlag_;				//タイムリセットフラグ
	double time_;				//クロック関数で時間を保存
	double timeBGM_;			//クロック関数で時間を保存(BGM用)
public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	DescriptionScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//開発者用コマンド
	void DeveloperCommands();

	//曲をループさせる
	void MusicLoop();

	//次のシーンへ
	void ChangeScene();

	//Spaceの描画のための
	void PushSwitching();

	//Timeをリセットする
	void TimeClockReset();

	//背景動かす処理
	void BackGroundMove();

	//描画
	void Draw() override;

	//開放
	void Release() override;
};