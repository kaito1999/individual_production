#include<time.h>
#include "Player.h"
#include "Engine/ResouceManager/Model.h"
#include "Engine/ResouceManager/Audio.h"
#include "Engine/gameObject/Camera.h"
#include "muzzle.h"
#include "PlayScene.h"
#include "Engine/ResouceManager/Image.h"


//コンストラクタ
Player::Player(IGameObject * parent)
	:IGameObject(parent, "Player"), hBulletModel_(-1),
	swingSpeed_(1.5f),hp_(10),hpCom_(10), power_(FALSE),frame_(0), preFrame_(0),
	powerUp_(FALSE), pwCount_(0), damageFlag_(FALSE), time_(0)
{
}

//デストラクタ
Player::~Player()
{
}

//初期化
void Player::Initialize()
{
	

	std::string fileName[] =
	{
		"data/Model/Player.fbx",
		"data/Model/PWPlayer.fbx",
		"data/Model/damagePlayer.fbx",
		
	};
	//登録したモデルを読み込み
	for (int i = 0; i < 3; i++)
	{
		hModel_[i] = Model::Load(fileName[i]);
	}

	//カメラ
	Camera* pCamera = CreateGameObject<Camera>(this);
	pCamera->SetPosition(D3DXVECTOR3(0, 2, -3));
	pCamera->SetTarget(D3DXVECTOR3(0, 1, 0));

	//子供として砲台を作成
	CreateGameObject<muzzle>(this);

	//弾を撃ったときにロードすると処理落ちするので、予めロードしておく
	hBulletModel_ = Model::Load("data/Model/Bullet.fbx");
	assert(hBulletModel_ >= 0);

	position_ = D3DXVECTOR3(7, 0, 3); //初期位置


	pStage_ = (Stage*)FindObject("Stage");

	//当たり判定追加
	SphereCollider* collision = new SphereCollider(D3DXVECTOR3(0, 0.7f, 0), 1.2f);
	AddCollider(collision);

	//音源の読み込み
	std::string SoundfileName[] =
	{
		"data/Sound/beshot.wav",
	};
	//登録した音源を読み込み
	for (int i = 0; i < 1; i++)
	{
		hSound_[i] = Audio::Load(SoundfileName[i]);
	}
	

}

//更新
void Player::Update()
{
	
	//キャラの操作
	Move();
	frame_++;

	//ダメージを受けたら赤く点滅させる
	HitEffect();


	//自機を強化させる
	PowerUp();
	

}

//自機を強化させる
void Player::PowerUp()
{
	if (power_ == TRUE && powerUp_ == FALSE && Input::IsKeyDown(DIK_LSHIFT))
	{

		powerUp_ = TRUE;
		preFrame_ = frame_;
		pwCount_++;
	}
	if ((frame_ - preFrame_) >= 240 && powerUp_ == TRUE)
	{
		power_ = FALSE;
		powerUp_ = FALSE;
		pwCount_--;
	}
}

//ダメージを受けたら赤く点滅させる
void Player::HitEffect()
{
	if (damageFlag_ == TRUE)
	{
		//ダメージを受けたモデルの番号
		pwCount_ = 2;
		
	}
	if (clock() - time_ >= FLASHING_TIME)
	{
		//ダメージを受けたモデルの番号
		if (powerUp_ == FALSE)
		{
			pwCount_ = 0;
		}
		else
		{
			pwCount_ = 1;
		}
	}
}

//キャラの操作
void Player::Move()
{

	D3DXMATRIX move;
	D3DXMatrixRotationY(&move, D3DXToRadian(rotate_.y));

	if (powerUp_ == TRUE )
	{
		 uv_ = D3DXVECTOR3(0, 0, 0.3f);
		D3DXVec3TransformCoord(&uv_, &uv_, &move);

		 lr_ = D3DXVECTOR3(0.3f, 0, 0);
		D3DXVec3TransformCoord(&lr_, &lr_, &move);
	}
	else
	{
		 uv_ = D3DXVECTOR3(0, 0, 0.1f);
		D3DXVec3TransformCoord(&uv_, &uv_, &move);

		 lr_ = D3DXVECTOR3(0.1f, 0, 0);
		D3DXVec3TransformCoord(&lr_, &lr_, &move);
	}



	//動く前の位置設定
	//ウォールとほぼセットになっている
	D3DXVECTOR3 prePos = position_;

	
	//コントローラーの処理
	//if (Input::IsPadButton(XINPUT_GAMEPAD_A))

	//{
	//	D3DXVECTOR3 stick = Input::GetPadStickL();
	//	position_.x += (stick.x / 5.0f);
	//	position_.z += (stick.y / 5.0f);
	//}

	////通常移動
	//else
	//{
	//	//スティックの倒した方向に進む
	//	D3DXVECTOR3 stick = Input::GetPadStickL();

	//	//そのままだと早すぎるので割って遅くする
	//	position_.x += (stick.x / 10.0f);
	//	position_.z += (stick.y / 10.0f);
	//}


		//通常移動
		if (Input::IsKey(DIK_W))
		{
			position_ += uv_;
		}

		else if (Input::IsKey(DIK_S))
		{
			position_ -= uv_;
		}
		
		else if (Input::IsKey(DIK_D))
		{
			position_ += lr_ ;
		}
		else if (Input::IsKey(DIK_A))
		{
			position_ -= lr_ ;
		}

		//右旋回
		if (Input::IsKey(DIK_RIGHT))
		{
			rotate_.y += swingSpeed_;
		}
		//左旋回
		if (Input::IsKey(DIK_LEFT))
		{
			rotate_.y -= swingSpeed_;
		}

		//ここでジャンプの処理やります
	//コントローラーのBボタンを押した
		if (Input::IsPadButton(XINPUT_GAMEPAD_B) || Input::IsKey(DIK_C))
		{
			if (position_.y < 2 )
			{
				position_.y += 0.1f;
			}
		}
		//ジャンプボタンが押されていない間は常に落ち続けている
		else
		{
			if (position_.y > 0.1)
			{
				position_.y -= 0.1f;
			}
		}




	//壁に対する当たり判定
	Wall(prePos);
}

//キャラが壁とぶつかったときの判定	
void Player::Wall(D3DXVECTOR3 &prePos)
{
	

	//どっちに移動したか計算する
	//差分を求める
	D3DXVECTOR3 move = position_ - prePos;

	//ベクトルの長さが伸びているかどうかで
	//動いているかどうかを判断する
	float speed = D3DXVec3Length(&move);


	//動いているかどうかで
	//ここから向き+衝突判定
	if (speed != 0)
	{

		//プレイヤーが最初に向いてる向き
		D3DXVECTOR3 front = D3DXVECTOR3(0, 0, 1);

		//内積を求めるために
		//ベクトルを正規化する（長さを１にすること）
		D3DXVec3Normalize(&move, &move);

		float dot = D3DXVec3Dot(&front, &move);
		float angle = acos(dot);


		D3DXVECTOR3 cross;
		D3DXVec3Cross(&cross, &move, &front);


		if (cross.y > 0)
		{
			angle *= -1;
		}

		if (Input::IsKey(DIK_W))
		{
			rotate_.y = D3DXToDegree(angle);
		}

		if (position_.x < 1.3| position_.z < 1.3 || position_.x > 13.7 || position_.z > 13.7)
		{
			position_ = prePos;
		}

		int checkX = (int)(position_.x - 0.3f);
		int checkZ = (int)position_.z;



		//ここで壁の上に乗れるようにしたい
		if (pStage_->IsWall(checkX, checkZ) == true && position_.y >= 1)
		{
			position_.y -= 0.05f;
		}
		//プレイヤーの高さが1より低かったら壁の当たり判定を出現させる
		else if (position_.y <= 1)
		{

			//checkX = (int)(position_.x - 0.3f);
			//checkZ = (int)position_.z;
			//もしプレイヤーの位置が壁だったらプレイヤーの位置をひとつ前の位置に戻す
			//このtrueは無くてもいい
			//左の当たり判定
			if (pStage_->IsWall(checkX, checkZ) == true)
			{


				//一個前の位置に戻す
				position_.x = checkX + 1.3;
			}





			checkX = (int)(position_.x + 0.3f);
			checkZ = (int)position_.z;

			//右の当たり判定
			if (pStage_->IsWall(checkX, checkZ) == true)
			{


				//一個前の位置に戻す
				position_.x = checkX - 0.3;
			}

			checkX = (int)position_.x;
			checkZ = (int)(position_.z + 0.3f);

			//上の当たり判定
			if (pStage_->IsWall(checkX, checkZ) == true)
			{


				//一個前の位置に戻す
				position_.z = checkZ - 0.3;
			}


			checkX = (int)position_.x;
			checkZ = (int)(position_.z - 0.3f);



			//画面下の当たり判定
			//パックマンの下向きであり重力が向いてる方向ではない
			if (pStage_->IsWall(checkX, checkZ) == true)

			{

				//一個前の位置に戻す

				position_.z = checkZ + 1.3;

			}


		}

	}

}


//描画
void Player::Draw()
{
	Model::SetMatrix(hModel_[pwCount_], _localMatrix);
	Model::Draw(hModel_[pwCount_]);
}

//開放
void Player::Release()
{
	//弾のモデルを解放
	Model::Release(hBulletModel_);

	//音楽を止める
	Audio::Stop(hSound_[0]);
}


//何かに当たった
void Player::OnCollision(IGameObject * pTarget)
{
	//弾に当たった
	if (pTarget->GetObjectName() == "EnemyBullet")
	{
		//HPの減少とほかのクラスで見る用のHPComの減少
		hp_--;
		hpCom_ = hp_;


		//効果音を鳴らす
		Audio::Play(hSound_[0]);

		//キャラクターをダメージを受けたときに赤くする処理
		{
			damageFlag_ = TRUE;
			//タイムをリセット
			time_ = clock();

		}
		
		
		if (hp_ == 0)
		{
			//ゲームオーバーシーンへ
			SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
			pSceneManager->ChangeScene(SCENE_ID_GAMEOVER);
			//自分を消す
			KillMe();
		}
		//相手（弾）を消す
		pTarget->KillMe();

	}


}