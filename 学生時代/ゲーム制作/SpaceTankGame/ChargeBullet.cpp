#include "ChargeBullet.h"
#include "Engine/ResouceManager/Model.h"

//コンストラクタ
ChargeBullet::ChargeBullet(IGameObject * parent)
	:IGameObject(parent, "ChargeBullet"), hModel_(-1), move_(D3DXVECTOR3(0, 0, 0)), dy_(0.0f),
	speed_(0.5f)
{
}

//デストラクタ
ChargeBullet::~ChargeBullet()
{
}

//初期化
void ChargeBullet::Initialize()
{
	//モデルデータのロード
	//仮で赤いプレイヤー出しておく
	hModel_ = Model::Load("data/Model/PWPlayer.fbx");
	assert(hModel_ >= 0);

	//衝突判定
	SphereCollider* collision = new SphereCollider(D3DXVECTOR3(0, 0, 0), 0.5f);
	AddCollider(collision);
}

//更新
void ChargeBullet::Update()
{
	//いずれかメインがビームライフルになるかも
	//落下
	Landing();

	//ある程度低い位置まで落ちたら消す
	Dropdown();
}

void ChargeBullet::Landing()
{
	//移動
	position_ += move_;
	move_.y += dy_;
	dy_ -= 0.003f;
}

void ChargeBullet::Dropdown()
{
	//ある程度低い位置まで落ちたら消す
	if (position_.y < DELETE_POSITION)
	{
		KillMe();
	}
}

//描画
void ChargeBullet::Draw()
{
	Model::SetMatrix(hModel_, _worldMatrix);
	Model::Draw(hModel_);
}

//開放
void ChargeBullet::Release()
{
}


//発射
void ChargeBullet::Shot(D3DXVECTOR3 position, D3DXVECTOR3 direction)
{
	//位置
	position_ = position;

	//移動は、引数で渡された方向にSpeedの速さ
	D3DXVec3Normalize(&move_, &direction);
	move_ *= speed_;
}