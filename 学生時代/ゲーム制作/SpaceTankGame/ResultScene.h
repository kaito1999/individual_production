#pragma once
#include "Engine/global.h"

//リザルトシーンを管理するクラス
class ResultScene : public IGameObject
{
	//定数宣言
	const unsigned int ONE_LOOP = 177000;					//曲一回分2分57秒
	const short INITIAL_POS = -1200;						//初期位置
	const unsigned short MAX_VAL = 0;						//最大値
	const unsigned short RESET = 0;							//リセット
	const unsigned short RESULT_HEIGHT = 100;				//RESULT画面の高さ調整
	const unsigned short CONTINUE_SELECT_SIDE = 150;		//つづける表示の横の調整
	const unsigned short CONTINUE_SELECT_HEIGHT = 500;		//つづける表示の高さ調整
	const unsigned short TITLE_SELECT_SIDE = 700;			//タイトルへの画像の横の位置
	const unsigned short TITLE_SELECT_HEIGHT = 500;			//タイトルへの画像の高さの位置
	const unsigned short SELECT_HEIGHT = 350;				//Selectの画像の高さ
	const unsigned short SELECT_LEFT = 150;					//Selectの画像の左に描画しているときの位置
	const unsigned short SELECT_RIGHT = 680;				//Selectの画像の右に描画しているときの位置


	int hSound_[3];					//音源番号
	int hImage_[7];					//画像番号
	bool continueFlag_;				//選択しているほうの管理
	int sideMove_;					//横の移動量
	int heightMove_;				//縦の移動量
	bool timeFlag_;					//タイムリセットフラグ
	double timeBGM_;				//クロック関数で時間を保存
public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	ResultScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//開発者用コマンド
	void DeveloperCommands();

	//次のシーンへ
	void ChangeScene();

	//開発者用コマンド
	void MusicLoop();

	//Timeをリセットする
	void TimeClockReset();

	//画像を動かす処理
	void BackGroundMove();

	//描画
	void Draw() override;

	//開放
	void Release() override;

};
