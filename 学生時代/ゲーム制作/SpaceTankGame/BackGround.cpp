#include "BackGround.h";
#include "Engine/ResouceManager/Model.h";
#include "Engine/ResouceManager/Image.h"
//コンストラクタ
BackGround::BackGround(IGameObject * parent)
	:IGameObject(parent, "BackGround"), hModel_(-1)
{
}

//デストラクタ
BackGround::~BackGround()
{
}

//初期化
void BackGround::Initialize()
{
	////モデルデータのロード
	hModel_ = Model::Load("data/Model/BackGround.fbx");
	assert(hModel_ >= 0);

	//サイズと
	 _scale  = D3DXVECTOR3(-10, -10, -10);
	 rotate_ = D3DXVECTOR3(100, 0, 0);
}

//更新
void BackGround::Update()
{
	//常時回転させたい
	KeepSpinning();
}

//常時回転させたい
void BackGround::KeepSpinning()
{
	//常時回転させておきたい
	rotate_ -= D3DXVECTOR3(0, 0.1f, 0);
}

//描画
void BackGround::Draw()
{
	Model::SetMatrix(hModel_, _worldMatrix);
	Model::Draw(hModel_);
	
}

//開放
void BackGround::Release()
{
}