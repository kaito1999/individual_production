#include "Water.h"
#include "Engine/ResouceManager/Model.h"

//コンストラクタ
Water::Water(IGameObject * parent)
	:IGameObject(parent, "Trans"), hModel_(-1), pEffect_(nullptr), pToonTex_(nullptr)//,pTexture(nullptr)
{
}

//デストラクタ
Water::~Water()
{
	//SAFE_RELEASE(pTexture);
	SAFE_RELEASE(pEffect_);
	SAFE_RELEASE(pToonTex_);
}

//初期化
void Water::Initialize()
{

	//HLSLファイルからシェーダを作成
	LPD3DXBUFFER err = 0;//これでシェーダのエラーを表示する　
	if (FAILED(D3DXCreateEffectFromFile(Direct3D::_pDevice, "WaterShader.hlsl", NULL, NULL, D3DXSHADER_DEBUG, NULL, &pEffect_, &err)))
	{
		MessageBox(NULL, (char*)err->GetBufferPointer(), "シェーダーエラー", MB_OK);
	}


	//モデルデータのロード
	hModel_ = Model::Load("Data/water.fbx", pEffect_);
	assert(hModel_ >= 0);


	D3DXCreateTextureFromFileEx(Direct3D::_pDevice, "data\\water_diff.png", 0, 0, 0, 0, D3DFMT_UNKNOWN, D3DPOOL_DEFAULT, D3DX_FILTER_NONE, D3DX_DEFAULT, NULL, NULL, NULL, &pToonTex_);


	//出す位置と大きさを調整
	position_.x += 7.5;
	position_.z += 7.5;
	_scale.x *= 1.5;
	_scale.z *= 1.5;

}

//更新
void Water::Update()
{
}


//描画
void Water::Draw()
{//                           ワールド行列
	Model::SetMatrix(hModel_, _worldMatrix);

	//ビュー行列
	D3DXMATRIX view;
	Direct3D::_pDevice->GetTransform(D3DTS_VIEW, &view);
	//プロジェクション行列
	D3DXMATRIX proj;
	Direct3D::_pDevice->GetTransform(D3DTS_PROJECTION, &proj);

	//ドーナツの情報　今どっち向いてるか　とか　かたむきは？とかを掛け算で出している
	D3DXMATRIX matWVP = _worldMatrix * view * proj;


	//オブジェクトの位置を法線に合わせて
	pEffect_->SetMatrix("WVP", &matWVP);

	//回転行列
	D3DXMATRIX rotateX, rotateY, rotateZ;
	D3DXMatrixRotationX(&rotateX, D3DXToRadian(rotate_.x));
	D3DXMatrixRotationY(&rotateY, D3DXToRadian(rotate_.y));
	D3DXMatrixRotationZ(&rotateZ, D3DXToRadian(rotate_.z));

	//拡大縮小
	D3DXMATRIX scale;
	D3DXMatrixScaling(&scale, _scale.x, _scale.y, _scale.z);
	//逆行列をかけるにはD3DXMatrixInverseを使う
	D3DXMatrixInverse(&scale, nullptr, &scale);


	D3DXMATRIX mat = scale * rotateZ * rotateX * rotateY; //ここで


	pEffect_->SetMatrix("RS", &mat);


	D3DLIGHT9 lightState;

	Direct3D::_pDevice->GetLight(0, &lightState);

	pEffect_->SetVector("LIGHT_DIR", (D3DXVECTOR4*)&lightState.Direction);

	//pEffect_->SetVector("DIFFUSE_COLOR", &D3DXVECTOR4(0,1,0,1));



	//カメラの位置
	pEffect_->SetVector("CAMERA_POS", (D3DXVECTOR4*)&D3DXVECTOR3(0, 5, -10));

	//ワールド行列
	pEffect_->SetMatrix("W", &_worldMatrix);

	pEffect_->SetTexture("TEXTURE_TOON", pToonTex_);

	//ココから描画開始
	pEffect_->Begin(NULL, 0);



	//uvスクロール ほんとは動いてないけどシェーダを使って動いているように見えるようにすること
	static float scroll = 0.0f;
	scroll += 0.0005;
	pEffect_->SetFloat("SCROLL", scroll);



	//輪郭を表示
	pEffect_->BeginPass(1);
	//                                                        裏面表示
	Direct3D::_pDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_CW);
	//Direct3D::_pDevice->SetRenderState(D3DRS_ZWRITEENABLE,FALSE);
	Model::Draw(hModel_);
	//                                                        デフォルト表示
	Direct3D::_pDevice->SetRenderState(D3DRS_CULLMODE, D3DCULL_CCW);
	//Direct3D::_pDevice->SetRenderState(D3DRS_ZWRITEENABLE, TRUE);
	pEffect_->EndPass();


	//普通に表示
	pEffect_->BeginPass(0);

	Model::Draw(hModel_);
	pEffect_->EndPass();

	pEffect_->End();
}

//開放
void Water::Release()
{

}