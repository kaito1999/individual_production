#include "EnemyBullet.h"
#include "Engine/ResouceManager/Model.h"

//コンストラクタ
EnemyBullet::EnemyBullet(IGameObject * parent)
	:IGameObject(parent, "EnemyBullet"), hModel_(-1), move_(D3DXVECTOR3(0, 0, 0)), dy_(0.0f),
	speed_(0.5f)
{
}

//デストラクタ
EnemyBullet::~EnemyBullet()
{
}

//初期化
void EnemyBullet::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("data/Model/Bullet.fbx");
	assert(hModel_ >= 0);

	//衝突判定
	SphereCollider* collision = new SphereCollider(D3DXVECTOR3(0, 0, 0), 0.5f);
	AddCollider(collision);
}

//更新
void EnemyBullet::Update()
{
	//落下
	Landing();

	//ある程度低い位置まで落ちたら消す
	Dropdown();
}

void EnemyBullet::Landing()
{
	//移動
	position_ += move_;
	move_.y += dy_;
	dy_ -= 0.0001f;
}

void EnemyBullet::Dropdown()
{
	//ある程度低い位置まで落ちたら消す
	if (position_.y < DELETE_POSITION)
	{
		KillMe();
	}
}

//描画
void EnemyBullet::Draw()
{
	Model::SetMatrix(hModel_, _worldMatrix);
	Model::Draw(hModel_);
}

//開放
void EnemyBullet::Release()
{
}


//発射
void EnemyBullet::Shot(D3DXVECTOR3 position, D3DXVECTOR3 direction)
{
	//位置
	position_ = position;

	//移動は、引数で渡された方向にspeedの速さ
	D3DXVec3Normalize(&move_, &direction);
	move_ *= speed_;

	move_ /= 4;
}