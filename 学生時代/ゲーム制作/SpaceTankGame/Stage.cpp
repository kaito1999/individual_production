#include "Stage.h"
#include "Engine/ResouceManager/Model.h"
#include "Engine/ResouceManager/CsvReader.h"
#include "Engine/gameObject/Camera.h"



//コンストラクタ
Stage::Stage(IGameObject * parent)
	:IGameObject(parent, "Stage")
{
}


//デストラクタ
Stage::~Stage()
{
}


//初期化
void Stage::Initialize()
{
	/*Camera* pCamera = CreateGameObject<Camera>(this);
	pCamera->SetPosition(D3DXVECTOR3(5, 15, -5));
	pCamera->SetTarget(D3DXVECTOR3(5, 3, 7));*/


	//モデルデータのロード
	hModel_[BL_FLOOR] = Model::Load("data/Model/Floor.fbx");
	assert(hModel_[BL_FLOOR] >= 0);

	//モデルデータのロード
	hModel_[BL_WALL] = Model::Load("data/Model/Wall.fbx");
	assert(hModel_[BL_WALL] >= 0);

	CsvReader csv;
	csv.Load("data/Map2.csv");


	for (int x = 0; x < 15; x++)
	{
		for (int z = 0; z < 15; z++)
		{
			table_[x][z] = csv.GetValue(x, z);
		}
	}

}



//更新
void Stage::Update()
{
}


//描画
void Stage::Draw()
{
	for (int x = 0; x < 15; x++)
	{
		for (int z = 0; z < 15; z++)
		{
			D3DXMATRIX mat;
			D3DXMatrixTranslation(&mat, x, 0, z);

			Model::SetMatrix(hModel_[table_[x][z]], mat);
			Model::Draw(hModel_[table_[x][z]]);

		}
	}
}


//開放
void Stage::Release()
{
}

