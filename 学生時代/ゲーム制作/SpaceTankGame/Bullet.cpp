#include "Bullet.h"
#include "Engine/ResouceManager/Model.h"

//コンストラクタ
Bullet::Bullet(IGameObject * parent)
	:IGameObject(parent, "Bullet"), hModel_(-1), move_(D3DXVECTOR3(0, 0, 0)), dy_(0.0f),
	speed_(0.5f)
{
}

//デストラクタ
Bullet::~Bullet()
{
}

//初期化
void Bullet::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("data/Model/Bullet.fbx");
	assert(hModel_ >= 0);

	//衝突判定
	SphereCollider* collision = new SphereCollider(D3DXVECTOR3(0, 0, 0), 0.5f);
	AddCollider(collision);
}

//更新
void Bullet::Update()
{
	//いずれかメインがビームライフルになるかも
	//落下
	Landing();

	//ある程度低い位置まで落ちたら消す
	Dropdown();
}

void Bullet::Landing()
{
	//移動
	position_ += move_;
	move_.y += dy_;
	dy_ -= 0.003f;
}

void Bullet::Dropdown()
{
	//ある程度低い位置まで落ちたら消す
	if (position_.y < DELETE_POSITION)
	{
		KillMe();
	}
}

//描画
void Bullet::Draw()
{
	Model::SetMatrix(hModel_, _worldMatrix);
	Model::Draw(hModel_);
}

//開放
void Bullet::Release()
{
}


//発射
void Bullet::Shot(D3DXVECTOR3 position, D3DXVECTOR3 direction)
{
	//位置
	position_ = position;

	//移動は、引数で渡された方向にspeedの速さ
	D3DXVec3Normalize(&move_, &direction);
	move_ *= speed_;

	
}