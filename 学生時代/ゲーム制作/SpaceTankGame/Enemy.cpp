#include "Enemy.h"
#include "Engine/ResouceManager/Model.h"
#include "Engine/ResouceManager/Audio.h"
#include "EnemyMuzzle.h"
#include "EnemyBullet.h"


//コンストラクタ
Enemy::Enemy(IGameObject * parent)
	:IGameObject(parent, "Enemy"), hModel_(-1)
	,hEnemyBulletModel_(-1),hp_(15), moveCount_(0)
	, aiMove_(0), lifePict_(2),frame_(0)
	, changeSceneFlag_(false)
{
}

//デストラクタ
Enemy::~Enemy()
{
}

//初期化
void Enemy::Initialize()
{
	
	//画像を配列で管理
	std::string fileName[] =
	{
		"data/Model/EnemyRed.fbx",
		"data/Model/EnemyYellow.fbx",
		"data/Model/EnemyGreen.fbx",
	};

	//登録した画像を読み込み
	for (int i = 0; i < 3; i++)
	{
		hModel2_[i] = Model::Load(fileName[i]);
	}
	
	//子供として砲台を作成
	CreateGameObject<EnemyMuzzle>(this);

	//弾を撃ったときにロードすると処理落ちするので、予めロードしておく
	hEnemyBulletModel_ = Model::Load("data/Model/Bullet.fbx");
	assert(hEnemyBulletModel_ >= 0);


	//サイズ
	_scale /= 5;

	//初期位置
	position_ = D3DXVECTOR3(7, 0, 10); 

	//向き
	rotate_.y = rand() % 360;


	pStage_ = (Stage*)FindObject("Stage");

	//当たり判定追加
	SphereCollider* collision = new SphereCollider(D3DXVECTOR3(0, 0.7f, 0), 1.2f);
	AddCollider(collision);

	//音源の読み込み
	std::string SoundfileName[] =
	{
		"data/Sound/beshot.wav",
	};
	//登録した音源を読み込み
	for (int i = 0 ; i < 1 ; i++)
	{
		hSound_[i] = Audio::Load(SoundfileName[i]);
	}

	//エフェクト
	P3DInitData data;
	data.pDevice = Direct3D::_pDevice;
	pP3D_ = new P3DEngine(data);
	pP3D_->Load("Data/Model/attackeffect.p3b");
	
}

//更新
void Enemy::Update()
{
	//体力に応じた体力バーの変化
	LifeBarColor();

	//体力に応じた行動
	ActForStrength();

	//次のシーンへ
	ChangeScene();
	
}

//次のシーンへ
void Enemy::ChangeScene()
{
	//次のシーンへ
	if (changeSceneFlag_ == true)
	{
		//ゲームオーバーシーンへ
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_CLEAR);

	}
}


//体力に応じた行動
void Enemy::ActForStrength()
{
	//動きの処理
	if (hp_ >= 0 && frame_ <= ONEHUNDREDEIGHTY_SECONDS)
	{
		Move();
	}
	if (hp_ <= 0 && frame_ >= TWOHUNDREDFORTY_SECONDS)
	{
		//シーンを切り替えるために使う
		changeSceneFlag_ = true;

	}
	else if (hp_ <= 0 && frame_ >= ONEHUNDREDEIGHTY_SECONDS && frame_ < TWOHUNDREDFORTY_SECONDS)
	{
		rotate_.x = 90;
		frame_++;
	}
	else if (hp_ <= 0)
	{
		rotate_.y -= 45;
		frame_++;
	}
}

//体力に応じた体力バーの変化
void Enemy::LifeBarColor()
{
	//ここでHPが減ったらHPバーを変わるようにした
	switch (hp_ / 6)
	{
	case 0: lifePict_ = 0; break;
	case 1: lifePict_ = 1; break;
	case 2: lifePict_ = 2; break;
	}
}

//敵の動き
void Enemy::Move()
{

	//動く前の位置設定
	//ウォールとほぼセットになっている
	D3DXVECTOR3 prePos = position_;

	moveCount_++;
	//ランダムで数を取得して1〜8までが出るようにした
	aiMove_ = rand() % 9;

	//ランダムで行動を決める
	if (moveCount_ == 100)
	{
		switch (aiMove_)
		{
		case 0: position_.x++; 
			    break;
		case 1: position_.z++; 
			    break;
		case 2: position_.x--; 
			    break;
		case 3: position_.z--;
			    break;
		case 4: rotate_.y += 15; 
			    break;
		case 5: rotate_.y -= 15; 
			    break;
		case 6: rotate_.y += 45;
				break;
		case 7: rotate_.y -= 45;
				break;
		//何もしない
		case 8: break;
		}
		moveCount_ = 0;
	}
	//壁に対する当たり判定
	Wall(prePos);

}
//壁に対する当たり判定
void Enemy::Wall(D3DXVECTOR3 &prePos)
{


	//どっちに移動したか計算する
	//差分を求める
	D3DXVECTOR3 move = position_ - prePos;

	//ベクトルの長さが伸びているかどうかで
	//動いているかどうかを判断する
	float speed = D3DXVec3Length(&move);


	//動いているかどうかで
	//ここから向き+衝突判定
	if (speed != 0)
	{

		//プレイヤーが最初に向いてる向き
		D3DXVECTOR3 front = D3DXVECTOR3(0, 0, 1);

		//内積を求めるために
		//ベクトルを正規化する（長さを１にすること）
		D3DXVec3Normalize(&move, &move);

		float dot = D3DXVec3Dot(&front, &move);
		float angle = acos(dot);


		D3DXVECTOR3 cross;
		D3DXVec3Cross(&cross, &move, &front);


		if (cross.y > 0)
		{
			angle *= -1;
		}

		
		rotate_.y = D3DXToDegree(angle);
		

		//壁より先に行ったらひとつ前の位置に戻す
		if (position_.x < 1.3 || position_.z < 1.3|| position_.x > 13.7 || position_.z > 13.7)
		{
			position_ = prePos;
		}

		
		int checkX = (int)(position_.x - 0.3);
		int checkZ = (int)position_.z;
		//checkX = (int)(position_.x - 0.3f);
		//checkZ = (int)position_.z;
		//もしプレイヤーの位置が壁だったらプレイヤーの位置をひとつ前の位置に戻す
		//このtrueは無くてもいい
		//左の当たり判定
		if (pStage_->IsWall(checkX, checkZ) == true)
		{
			//一個前の位置に戻す
			position_.x = checkX + 1.3;
		}


		checkX = (int)(position_.x + 0.3f);
		checkZ = (int)position_.z;

		//右の当たり判定
		if (pStage_->IsWall(checkX, checkZ) == true)
		{


			//一個前の位置に戻す
			position_.x = checkX - 1.3;
		}

		checkX = (int)position_.x;
		checkZ = (int)(position_.z + 0.3f);

		//上の当たり判定
		if (pStage_->IsWall(checkX, checkZ) == true)
		{

			//一個前の位置に戻す
			position_.z = checkZ - 1.3;
		}


		checkX = (int)position_.x;
		checkZ = (int)(position_.z - 0.3f);



		//画面下の当たり判定
		//敵の下向きであり重力が向いてる方向ではない
		if (pStage_->IsWall(checkX, checkZ) == true)
		{

			//一個前の位置に戻す

			position_.z = checkZ + 1.3;

		}

	}

	
}

//敵の打つ弾
void Enemy::Shot()
{
	IGameObject* playScene = FindObject("PlayScene");
	EnemyBullet* pEnemyBullet = CreateGameObject<EnemyBullet>(FindObject("PlayScene"));


	D3DXVECTOR3 shotPos = Model::GetBonePosition(hModel_, "ShotPoint");    //発射口
	D3DXVECTOR3 cannonRoot = Model::GetBonePosition(hModel_, "CannonRoot");//発生元
	pEnemyBullet->Shot(shotPos, (shotPos - cannonRoot));
	
}

//描画
void Enemy::Draw()
{
	

	Model::SetMatrix(hModel2_[lifePict_], _localMatrix );
	Model::Draw(hModel2_[lifePict_]);

	pP3D_->Draw();
}

//開放
void Enemy::Release()
{
	//弾のモデルを解放
	Model::Release(hEnemyBulletModel_);
	SAFE_DELETE(pP3D_);

	//音楽を止める
	Audio::Stop(hSound_[0]);
}


//何かに当たった
void Enemy::OnCollision(IGameObject * pTarget)
{
	////弾に当たった
	if (pTarget->GetObjectName() == "Bullet" || pTarget->GetObjectName() == "ChargeBullet")
	{
		hp_--;
		

		//ここで出す位置を決めれる
		pP3D_->Play("Data/Model/attackeffect.p3b", D3DXVECTOR3(position_.x, position_.y + 1, position_.z), FALSE);

		//効果音を鳴らす
		Audio::Play(hSound_[0]);

		//相手（弾）を消す
		pTarget->KillMe();

		
	}

}