#pragma once
#include "Engine/GameObject/GameObject.h"


//弾を管理するクラス
class EnemyBullet : public IGameObject
{

	const short DELETE_POSITION = -5; //弾が消える位置


	int hModel_;		//モデル番号
	D3DXVECTOR3 move_;	//移動ベクトル
	float dy_;			//Ｙ方向の加速度

	const float speed_;	//速度

public:
	EnemyBullet(IGameObject* parent);
	~EnemyBullet();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//落下
	void Landing();

	//ある程度低い位置まで落ちたら消す
	void Dropdown();

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//発射
	//引数；position	発射位置
	//引数：direction	発射方向
	void Shot(D3DXVECTOR3 position, D3DXVECTOR3 direction);

	
};