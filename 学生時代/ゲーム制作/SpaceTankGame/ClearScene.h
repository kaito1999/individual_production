#pragma once
#include "Engine/global.h"



//クリアシーンを管理するクラス
class ClearScene : public IGameObject
{
	//定数宣言
	const unsigned short FOUR_SECONDS = 4000;				//4秒
	const unsigned short ZEROPOINTFIVE_SECONDS = 500;		//0.5秒
	const short INITIAL_POS = -1200;						//初期位置
	const unsigned short  MAX_VAL = 0;						//最大値
	const unsigned short RESET = 0;							//リセット


private:
	int hSound_;				//サウンド番号
	int hImage_[4];				//画面に表示するうえで必要そうなものを、配列で管理する
	int sideMove_;				//横の移動量
	int heightMove_;			//縦の移動量
	bool timeFlag_;				//タイムリセットフラグ
	double time_;				//クロック関数で時間を保存
	double timeSub_;			//time_変数とほぼ同じ内容のものクロック関数で時間を保存
	bool frameChangeFlag_;		//画面の点滅がバグが起きないためにフラグを作って置く
public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	ClearScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//敵の悪あがき用の処理
	void Desperate();

	//開発者用コマンド
	void DeveloperCommands();

	//背景を動かす
	void BackGroundMove();

	//クロック関数を使ってタイム変数を初期化する
	void TimeClockReset();

	//描画
	void Draw() override;

	//開放
	void Release() override;
};