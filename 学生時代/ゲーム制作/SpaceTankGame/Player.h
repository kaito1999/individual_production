#pragma once
#include "Engine/GameObject/GameObject.h"
#include "Stage.h"
#include "PlayScene.h"



//プレイヤーを管理するクラス
class Player : public IGameObject
{
	//定数宣言
	const unsigned short FLASHING_TIME = 200;		//0.2秒

	int hModel_[3];				//画像の読み込み
	int hSound_[1];				//サウンド番号
	int hGroundModel_;			//地面のモデル番号
	int hBulletModel_;			//弾のモデル番号
	const float swingSpeed_;	//旋回速度
	D3DXVECTOR3 uv_;			//プレイヤーが動く上下のベクトル
	D3DXVECTOR3 lr_;			//プレイヤーが動く左右のベクトル


	Stage* pStage_;

	int frame_;					//updateで回してフレームとして使う
	int preFrame_;				//更新される1つ前のフレームを覚えておく
	int pwCount_;				//キャラクターのステータスを管理する
	int hp_;					//キャラクターのHPの処理
	bool damageFlag_;			//ダメージを受けた時の処理を担当する
	double time_;				//クロック関数で時間を保存

public:

	bool powerUp_;				//パワーアップ関連のバグが出ないようにする
	bool power_;				//アイテムを取ったかどうかをitemクラスとやり取りする

	//他クラスで使うHP参照用
	int hpCom_;

	

	//コンストラクタ
	Player(IGameObject* parent);

	//デストラクタ
	~Player();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	void PowerUp();

	void HitEffect();


	void PowerFlag(bool CPower)
	{
		power_ = CPower;
	}

	void PowerUpFlag(bool CPowerUp)
	{
		powerUp_ = CPowerUp;
	}

	//キャラの操作
	void Move();

	//キャラが壁とぶつかったときの判定
	void Wall(D3DXVECTOR3 &prePos);

	
	//描画
	void Draw() override;

	//開放
	void Release() override;


	//何かに当たった
	//引数：pTarget　当たった相手
	void OnCollision(IGameObject * pTarget) override;

};