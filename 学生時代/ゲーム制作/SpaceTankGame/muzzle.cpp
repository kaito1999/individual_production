#include "muzzle.h"
#include "Engine/gameObject/Camera.h"
#include "Engine/ResouceManager/Model.h"
#include "Bullet.h"
#include "ChargeBullet.h"
#include "Engine/ResouceManager/Image.h"
#include "Player.h"
#include "Engine/ResouceManager/Audio.h"

//コンストラクタ
muzzle::muzzle(IGameObject * parent)
	:IGameObject(parent, "muzzle"),hModel_(-1),coolTime_(0), 
	stock_(REMAINING_BULLET), chargeTime_(0), powerUpFlag_(FALSE),
	powerUpCount_(OUTSIDE_SCREEN), hSound_(-1)
{
}

//デストラクタ
muzzle::~muzzle()
{
}

//初期化
void muzzle::Initialize()
{
	//モデルデータのロード
	hModel_ = Model::Load("data/Model/TankHead.fbx");
	assert(hModel_ >= 0);
	

	//画像の管理
	std::string fileName[] =
	{
		"data/Picture/RemainingBulletsZero.png",
		"data/Picture/RemainingBulletsOne.png",
		"data/Picture/RemainingBulletsTwo.png",
		"data/Picture/RemainingBulletsThree.png",
		"data/Picture/RemainingBulletsFour.png",
		"data/Picture/RemainingBulletsFive.png",
		"data/Picture/PreCharge.png",
		"data/Picture/FullCharge.png",
		"data/Picture/ChargeBlack.png",
		"data/Picture/ChargeYellow.png",
	};
	//登録した画像を読み込み
	for (int i = 0; i < 10; i++)
	{
		hImage_[i] = Image::Load(fileName[i]);
	}

	/////////////////////////////////////////////////
	std::string PfileName[] =
	{
		"data/Picture/NotPower.png",
		"data/Picture/Power.png",
	};

	//登録した画像を読み込み
	for (int i = 0; i < 2; i++)
	{
		hPImage_[i] = Image::Load(PfileName[i]);
	}
	/////////////////////////////////////////////////
	
	 //サウンドデータのロード
	//フリー音源持ってきました
	hSound_ = Audio::Load("data/Sound/Gun.wav");
	assert(hSound_ >= 0);

}

//更新
void muzzle::Update()
{
	//リロード
	Reload();
	//弾を打つ
	Shot();
	//プレイヤーの強化の処理
	PowerUp();
}

//強化が可能かどうかを表示する
void muzzle::PowerUp()
{
	if (((Player*)GetParent())->powerUp_ == TRUE)
	{
		//パワーアップの残量のバーを動かす
		powerUpCount_++;
	}

	if (((Player*)GetParent())->power_ == TRUE)
	{

		if (powerUpFlag_ == FALSE)
		{
			//パワーアップのゲージバーをセットする
			powerUpCount_ = SET_GAUGE_BAR;
		}
		powerUpFlag_ = TRUE;
	}

	if (((Player*)GetParent())->power_ == FALSE)
	{
		powerUpFlag_ = FALSE;
	}
}

//弾のリロード
void muzzle::Reload()
{
	if (stock_ == 0)
	{
		coolTime_++;
		if (coolTime_ >= 180)//３秒リロード
		{
			stock_ = 5;
			coolTime_ = FRAME_MIN ;
		}
	}
}

//弾を撃つ
void muzzle::Shot()
{
	//chargeTimeが最大以外の時スペースキーを押すとchargeTimeが１Frameごとにたまる
	if (Input::IsKey(DIK_SPACE) && chargeTime_ < (FRAME_MAX))
	{
		chargeTime_++;
	}
	//chargeTimeが0以上の時かつSPACEキーが押されていないときchargeTimeが１Frameごとに減少する
	else if(!Input::IsKey(DIK_SPACE) && chargeTime_ > FRAME_MIN)
	{
		chargeTime_--;
	}
	

	if (Input::IsKeyDown(DIK_SPACE) && stock_ > FRAME_MIN )
	{
		Gun();
		Audio::Play(hSound_);
		stock_--;
	}
	if (Input::IsKeyUp(DIK_SPACE) && chargeTime_ >= (FRAME_MAX - 1))
	{

		//MagnumFlag = true;
		Audio::Play(hSound_);
		chargeTime_ = 0;
		for (int count = 0; count < 4; count++)
		{
			ChargeGun();
		}
	}
}

//強化された弾を打つ
void muzzle::ChargeGun()
{
	IGameObject* playScene = FindObject("PlayScene");
	ChargeBullet* pChargeBullet = CreateGameObject<ChargeBullet>(FindObject("PlayScene"));

	D3DXVECTOR3 shotPos = Model::GetBonePosition(hModel_, "ShotPoint");    //発射口
	D3DXVECTOR3 cannonRoot = Model::GetBonePosition(hModel_, "CannonRoot");//発生元
	pChargeBullet->Shot(shotPos, shotPos - cannonRoot);
}

//普通の弾を打つ
void muzzle::Gun()
{
	IGameObject* playScene = FindObject("PlayScene");
	Bullet* pBullet = CreateGameObject<Bullet>(FindObject("PlayScene"));

	D3DXVECTOR3 shotPos = Model::GetBonePosition(hModel_, "ShotPoint");    //発射口
	D3DXVECTOR3 cannonRoot = Model::GetBonePosition(hModel_, "CannonRoot");//発生元
	pBullet->Shot(shotPos, shotPos - cannonRoot);
}
//描画
void muzzle::Draw()
{
	Model::SetMatrix(hModel_, _worldMatrix);
	Model::Draw(hModel_);

	//バレットの画像の管理
	{
		D3DXMATRIX m;
		D3DXMatrixTranslation(&m, 1020, 530, 0);
		D3DXMATRIX m2;
		D3DXMatrixTranslation(&m2, 920, 630, 0);

		//バレットの残弾数表示
		Image::SetMatrix(hImage_[stock_], _localMatrix * m);
		Image::Draw(hImage_[stock_]);

	//チャージの表示
			if (chargeTime_ >= FRAME_MAX)
			{
				Image::SetMatrix(hImage_[7], _localMatrix * m2);
				Image::Draw(hImage_[7]);
			}
			else
			{
				Image::SetMatrix(hImage_[6], _localMatrix * m2);
				Image::Draw(hImage_[6]);
			}
	}

	//真っ黒いチャージタイム表示
	{
		D3DXMATRIX m3;
		D3DXMatrixTranslation(&m3, 1040, 650, 0);
		Image::SetMatrix(hImage_[8], _localMatrix * m3);
		Image::Draw(hImage_[8]);
	}
	
	D3DXMATRIX m4;
	D3DXMatrixTranslation(&m4, 1280 - (chargeTime_ * 4), 650, 0);
	Image::SetMatrix(hImage_[9], _localMatrix * m4);
	Image::Draw(hImage_[9]);
	////////////////////////////////////////////
	//パワーアップ可能かどうかの画像表示
	{
		if (powerUpFlag_ == TRUE)
		{
			D3DXMATRIX m5;
			D3DXMatrixTranslation(&m5, 1030, 400, 0);

			Image::SetMatrix(hPImage_[1], _localMatrix * m5);
			Image::Draw(hPImage_[1]);
		}
		else
		{
			D3DXMATRIX m5;
			D3DXMatrixTranslation(&m5, 1030, 400, 0);

			Image::SetMatrix(hPImage_[0], _localMatrix * m5);
			Image::Draw(hPImage_[0]);
		}
	}
	///////////////////////////////////////////////////
	//フルチャージの文字
	D3DXMATRIX m6;
	D3DXMatrixTranslation(&m6, 1040, 520, 0);

	Image::SetMatrix(hImage_[8], _localMatrix * m6);
	Image::Draw(hImage_[8]);

	//powerUpCountで少しずつ画面外に出ていくようになっている
	D3DXMATRIX m7;
	D3DXMatrixTranslation(&m7, powerUpCount_, 520, 0);

	Image::SetMatrix(hImage_[9], _localMatrix * m7);
	Image::Draw(hImage_[9]);
}

//開放
void muzzle::Release()
{
	
	
}