#pragma once

#include "Engine/global.h"

//開発者用コマンドシーンを管理するクラス
class DeveloperCommandsScene : public IGameObject
{
	int hImage_[1];		//画面に表示するうえで必要なものを、配列で管理する

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	DeveloperCommandsScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//シーン切り替え
	void SceneSwitching();

	//描画
	void Draw() override;

	//開放
	void Release() override;
};