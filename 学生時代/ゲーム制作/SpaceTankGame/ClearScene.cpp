#include<time.h>
#include "ClearScene.h"
#include "Engine/ResouceManager/Image.h"
#include "Engine/ResouceManager/Audio.h"

//コンストラクタ
ClearScene::ClearScene(IGameObject * parent)
	: IGameObject(parent, "ClearScene")
	, timeFlag_(false), time_(0), timeSub_(0)
	, sideMove_(0), heightMove_(INITIAL_POS)
	, hSound_(-1), frameChangeFlag_(false)
{
}

//初期化
void ClearScene::Initialize()
{

	//画像の管理
	std::string fileName[] =
	{
		"data/Picture/Galaxy.png",
		"data/Picture/gameclear.png",
		"data/Picture/YellowFrame.png",
		"data/Picture/YellowFrameDark.png",
	};
	//登録した画像を読み込み
	for (int i = 0; i < 4; i++)
	{
		hImage_[i] = Image::Load(fileName[i]);
	}

	//サウンドデータのロード
	hSound_ = Audio::Load("data/Sound/GAMECLEAR.wav");
	assert(hSound_ >= 0);
	//再生
	Audio::Play(hSound_);
}

//更新
void ClearScene::Update()
{
	//開発者用コマンド
	DeveloperCommands();

	//背景動かす処理
	BackGroundMove();

	//クロック関数を使ってタイム変数を初期化する
	TimeClockReset();

	//敵の悪あがき用の処理
	Desperate();

}
//敵の悪あがき用の処理
void ClearScene::Desperate()
{
	//2秒くらい経った時
	if (clock() - time_ >= FOUR_SECONDS)
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_RESULT);
	}
	//0.5秒おきに点滅させる
	if (clock() - timeSub_ >= ZEROPOINTFIVE_SECONDS && frameChangeFlag_ == FALSE)
	{
		frameChangeFlag_ = TRUE;
		//タイムをリセット
		timeSub_ = clock();
	}
	if (clock() - timeSub_ >= ZEROPOINTFIVE_SECONDS && frameChangeFlag_ == TRUE)
	{
		frameChangeFlag_ = FALSE;
		//タイムをリセット
		timeSub_ = clock();
	}
}
//開発者用コマンド
void ClearScene::DeveloperCommands()
{
	//QとMとP同時に押したら開発者用コマンドシーンへ
	if (Input::IsKey(DIK_Q) && Input::IsKey(DIK_M) && Input::IsKey(DIK_P))
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_DEVELOPERCOMMANDS);
	}
}

//背景動かす処理
void ClearScene::BackGroundMove()
{
	//背景の位置を動かす
	{
		sideMove_--;
		heightMove_++;
	}

	//背景の大きさぎりぎりまで来たら初期位置に戻す
	if (heightMove_ > MAX_VAL)
	{
		sideMove_ = RESET;
		heightMove_ = INITIAL_POS;
	}
}
// クロック関数を使ってタイム変数を初期化する
void ClearScene::TimeClockReset()
{
	
	//リセットされてないなら
	if (!timeFlag_)
	{
		//タイムをリセット
		time_ = clock();
		//フラグをオンで二回目の初期化をしない
		timeFlag_ = true;
	}
}

//描画
void ClearScene::Draw()
{

	D3DXMATRIX m;
	D3DXMatrixTranslation(&m, sideMove_, heightMove_, 0);
	//背景表示
	Image::SetMatrix(hImage_[0], _localMatrix*m);
	Image::Draw(hImage_[0]);
	//ゲームクリアの文字を表示
	Image::SetMatrix(hImage_[1], _localMatrix);
	Image::Draw(hImage_[1]);

	if (frameChangeFlag_ == FALSE)
	{
		//薄い黄色の枠を表示
		Image::SetMatrix(hImage_[2], _localMatrix);
		Image::Draw(hImage_[2]);
	}
	else
	{
		//濃い黄色の枠を表示
		Image::SetMatrix(hImage_[3], _localMatrix);
		Image::Draw(hImage_[3]);
	}
	
}

//開放
void ClearScene::Release()
{
}