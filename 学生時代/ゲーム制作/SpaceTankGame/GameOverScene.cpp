#include<time.h>
#include "GameOverScene.h"
#include "Engine/ResouceManager/Image.h"
#include "Engine/ResouceManager/Audio.h"

//コンストラクタ
GameOverScene::GameOverScene(IGameObject * parent)
	: IGameObject(parent, "GameOverScene")
	, timeFlag_(false), time_(0), sideMove_(0), heightMove_(INITIAL_POS)
	, hSound_(-1)
{
}

//初期化
void GameOverScene::Initialize()
{

	//画像の管理
	std::string fileName[] =
	{
		"data/Picture/GalaxyRed.png",
		"data/Picture/gameover.png",
	};
	//登録した画像を読み込み
	for (int i = 0; i < 2; i++)
	{
		hImage_[i] = Image::Load(fileName[i]);
	}

	//サウンドデータのロード
	hSound_ = Audio::Load("data/Sound/GAMEOVER.wav");
	assert(hSound_ >= 0);
	//再生
	Audio::Play(hSound_);
}

//更新
void GameOverScene::Update()
{
	//開発者用コマンド
	DeveloperCommands();

	//背景を動かす処理
	BackGroundMove();

	//クロック関数を使ってタイム変数を初期化する
	TimeClockReset();

	//時間でシーン切り替え
	ChangeScene();
	
}

//時間でシーン切り替え
void GameOverScene::ChangeScene()
{
	//6秒くらい経った時
	if (clock() - time_ >= SIX_SECONDS)
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_RESULT);
	}
}

//開発者用コマンド
void GameOverScene::DeveloperCommands()
{
	//QとMとP同時に押したら開発者用コマンドシーンへ
	if (Input::IsKey(DIK_Q) && Input::IsKey(DIK_M) && Input::IsKey(DIK_P))
	{
		SceneManager* pSceneManager = (SceneManager*)FindObject("SceneManager");
		pSceneManager->ChangeScene(SCENE_ID_DEVELOPERCOMMANDS);
	}
}

void GameOverScene::BackGroundMove()
{
	//背景の位置を動かす
	{
		sideMove_--;
		heightMove_++;
	}

	//背景の大きさぎりぎりまで来たら初期位置に戻す
	if (heightMove_ > MAX_VAL)
	{
		sideMove_ = RESET;
		heightMove_ = INITIAL_POS;
	}
}

// クロック関数を使ってタイム変数を初期化する
void GameOverScene::TimeClockReset()
{
	//リセットされてないなら
	if (!timeFlag_)
	{
		//タイムをリセット
		time_ = clock();
		//フラグをオンで二回目の初期化をしない
		timeFlag_ = true;
	}
}

//描画
void GameOverScene::Draw()
{
	D3DXMATRIX m;
	D3DXMatrixTranslation(&m, sideMove_, heightMove_, 0);
	//背景表示
	Image::SetMatrix(hImage_[0], _localMatrix*m);
	Image::Draw(hImage_[0]);

	//ゲームオーバーの画像
	Image::SetMatrix(hImage_[1], _localMatrix);
	Image::Draw(hImage_[1]);
}

//開放
void GameOverScene::Release()
{
	Audio::Stop(hSound_);
}


