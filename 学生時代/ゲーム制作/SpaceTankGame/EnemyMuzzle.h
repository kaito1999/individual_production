#pragma once
#include "Engine/GameObject/GameObject.h"


//マゼルを管理するクラス
class EnemyMuzzle : public IGameObject
{

	const unsigned short POSITIONED = 3;	//位置調整
	int hModel_;							//モデル番号

public:
	//コンストラクタ
	EnemyMuzzle(IGameObject* parent);

	//デストラクタ
	~EnemyMuzzle();

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//ランダムで球を打つか打たないか決める
	void BulletMove();

	//弾を撃つ
	void Shot();

	//描画
	void Draw() override;

	//開放
	void Release() override;

	//とりあえずフレームで時間を図る
	int Frame;

	//AIでどう動くかを決める
	int AIMove;
};
