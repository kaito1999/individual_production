#pragma once
#include "Engine/global.h"

//ゲームオーバーシーンを管理するクラス
class GameOverScene : public IGameObject
{
	//定数宣言
	const unsigned short SIX_SECONDS = 6000;		//6秒
	const short INITIAL_POS = -1200;				//初期位置
	const unsigned short MAX_VAL = 0;				//最大値
	const unsigned short RESET = 0;					//リセット

	int hSound_;			//サウンド番号
	int hImage_[2];			//画面に表示するうえで必要そうなものを、配列で管理する
	int sideMove_;			//横の移動量
	int heightMove_;		//縦の移動量
	bool timeFlag_;			//タイムリセットフラグ
	double time_;			//クロック関数で時間を保存

public:
	//コンストラクタ
	//引数：parent  親オブジェクト（SceneManager）
	GameOverScene(IGameObject* parent);

	//初期化
	void Initialize() override;

	//更新
	void Update() override;

	//時間でシーン切り替え
	void ChangeScene();

	//開発者用コマンド
	void DeveloperCommands();

	//クロック関数を使ってタイム変数を初期化する
	void BackGroundMove();

	//クロック関数を使ってタイム変数を初期化する
	void TimeClockReset();

	//描画
	void Draw() override;

	//開放
	void Release() override;
};
