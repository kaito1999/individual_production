﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ItemCorrection : MonoBehaviour
{
    // アイテム全種
    public enum ITEM_ID{
        APPLE,      // 小リンゴ
        BIGAPPLE,   // 大リンゴ
        MAX
    }

    // リスト
    public static List<ITEM_ID> itemList;


    
    void Start()
    {
        itemList = new List<ITEM_ID>();   
    }

    // 名前からIDを探す
    // 引数：名前、戻り値：ITEM_ID
    public static ITEM_ID NameToID(string name)
    {
        switch(name)
        {
            case "ItemPrefab2(Clone)": return ITEM_ID.APPLE;
            case "ItemBigPrefab(Clone)": return ITEM_ID.BIGAPPLE;
            default: return ITEM_ID.MAX;
        }
    }

    // IDから名前を探す
    public static string IDToName(ITEM_ID id)
    {
        switch (id)
        {
            case ITEM_ID.APPLE: return "小さいリンゴ";
            case ITEM_ID.BIGAPPLE: return "大きいリンゴ";
            case ITEM_ID.MAX: return null;
            default: return null;
        }
    }

    // バッグの中身を保存
    public static void BagSave(List<ITEM_ID> list)
    {
        itemList = new List<ITEM_ID>(list);
    }

}
