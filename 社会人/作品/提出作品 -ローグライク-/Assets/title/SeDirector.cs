﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeDirector : MonoBehaviour
{
    public AudioClip sound1;
    AudioSource audioSource;

    private bool seFlag;

    // Start is called before the first frame update
    void Start()
    {
        //画面遷移してもオブジェクトが壊れないようにする
        DontDestroyOnLoad(this);

        //Componentを取得
        audioSource = GetComponent<AudioSource>();

        seFlag = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (seFlag == false)
        {
            // 左
            if (Input.GetKeyDown(KeyCode.Z) || Input.GetKeyDown(KeyCode.Return))
            {
                //音(sound1)を鳴らす
                audioSource.PlayOneShot(sound1);

                seFlag = true;
            }
        }


    }
}
