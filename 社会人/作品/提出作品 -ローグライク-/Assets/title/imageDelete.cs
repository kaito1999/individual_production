﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class imageDelete : MonoBehaviour
{
    private int count_;

    // Start is called before the first frame update
    void Start()
    {
        count_ = 0;
    }

    // Update is called once per frame
    void Update()
    {
        count_++;
        if(count_ > 60)
        {
            Destroy(this.gameObject);
        }
    }
}
