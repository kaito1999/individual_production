﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class TitleDirector : MonoBehaviour
{
    // 変数
    public static int allEx_;     // 総合獲得経験値
    public static int exMax_;     // 経験値上限
    public static int hpMax_;     // 体力上限
    public static int floor_;      // 階層
    public static int hpSave_;    // 体力保持
    public static int exSave_;     // 経験値保持
    public static int atkSave_;   // 攻撃力保持
    public static int levelSave_;  // レベル保持

    /// <summary>
    public static bool trigger_;     //BGNのtriggerの保持
    public static int bgmCount_; 
    /// </summary>

    public static List<String> itemList_;

    void Start()
    {
        // ステータス初期化
        allEx_ = 0;
        exMax_ = 10;
        hpMax_ = 10;
        floor_ = 1;
        hpSave_ = 10;
        exSave_ = 0;
        atkSave_ = 5;
        levelSave_ = 1;
        trigger_ = false;
        bgmCount_ = 0;

        // アイテムリスト初期化
        itemList_ = new List<String>();
    }


    void Update()
    {
        //今現在の条件が左クリックしたらシーン移行
        //条件文かえればシーン変える部分も変わる
        if(Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.Z))
        {
            SceneManager.LoadScene("PlayScene");
        }
    }
}
