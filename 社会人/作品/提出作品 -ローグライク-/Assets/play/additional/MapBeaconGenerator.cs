﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapBeaconGenerator : MonoBehaviour
{
    //タグの数を数えるときに使う
    GameObject[] tagObjects;
    // 敵プレハブを格納するリスト
    List<GameObject> MapBeaconList_ = new List<GameObject>();

    //
    public GameObject MapBeaconPrefab;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // prefab名ではなくtagを入力する
        Check("Beacon");
        mapBeaconGenerator();
    }

   private void mapBeaconGenerator()
    {
        if (tagObjects.Length == 0)
        {
            GameObject Beacon_ = Instantiate(MapBeaconPrefab) as GameObject;

            //ランダムで出現させる
            int SpawnX_ = Random.Range(18, -20);
            int SpawnY_ = Random.Range(18, -20);

            //出す場所を決める
            Beacon_.transform.position = new Vector3(SpawnX_ + 0.5f, 2, SpawnY_ + 0.5f);
        }
    }

    //シーン上のBlockタグが付いたオブジェクトを数える
    void Check(string tagname)
    {
        tagObjects = GameObject.FindGameObjectsWithTag(tagname);

        MapBeaconList_.Clear();
        foreach (GameObject chara in tagObjects)
        {
            MapBeaconList_.Add(chara);
        }

    }
}
