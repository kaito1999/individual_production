﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour
{
    public enum Result_Contents
    {
        Enemy,
        Trap,
        TurnOver,
        Clear,
        Max
    }

    // Textオブジェクト
    public GameObject floor = null;
    public GameObject level = null;
    public GameObject health = null;
    public GameObject regain = null;
    public GameObject experience = null;

    public GameObject[] resultText;

    private GameObject panel_;
    public PlayerController player_;

    public bool bgmEndFlag_;    //soundに伝達してbgmを止める

    void Start()
    {
        bgmEndFlag_ = false;

        panel_ = GameObject.Find("ResultCanvas");
        player_ = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        resultText = new GameObject[]
        {
            GameObject.Find("Result1"),
            GameObject.Find("Result2"),
            GameObject.Find("Result3"),
            GameObject.Find("Result4")
        };

        Debug.Log("このオブジェクト名は" + panel_.name);
        foreach(GameObject text in resultText)
        {
            Debug.Log("このオブジェクト名は" + text.name);
        }

        panel_.SetActive(false);
    }

    void Update()
    {
        // コンポーネント取得
        Text floor_text = floor.GetComponent<Text>();
        Text level_text = level.GetComponent<Text>();
        Text health_text = health.GetComponent<Text>();
        Text regain_text = regain.GetComponent<Text>();
        Text experience_text = experience.GetComponent<Text>();


        // 表示内容を更新
        int print;

        // レベル
        print = player_.GetLevel();
        level_text.text = "Level:" + print;

        // 体力
        print = player_.GetHP();
        health_text.text = "HP:" + print + " / " + TitleDirector.hpMax_;

        // 巻き戻し
        print = player_.GetRegain();
        regain_text.text = "Regain:" + print + "/ 3";
        
        // 階層
        floor_text.text = "B" + TitleDirector.floor_ ;

        // 経験値
        print = player_.GetExperience();
        experience_text.text = "ex:" + print + " / " + TitleDirector.exMax_;

    }

    // リザルト画面
    // 第1引数 true:クリア / false:ゲームオーバー、 第2引数 プレイヤを倒した敵名
    public void ResultScreen(Result_Contents result, string enemyName = null)
    {
        panel_.SetActive(true);

        if (result == Result_Contents.Enemy)
        {
            enemyName = "スライム";

            resultText[0].GetComponent<Text>().text = "プレイヤーは";
            resultText[1].GetComponent<Text>().text = "ダンジョンの" + "B" + TitleDirector.floor_ + "階にて";
            resultText[2].GetComponent<Text>().text = enemyName + "に倒された";
            resultText[3].GetComponent<Text>().text = "Lv." + TitleDirector.levelSave_ + "総経験値:" + TitleDirector.allEx_;
        }
        else if (result == Result_Contents.Trap)
        {
            resultText[0].GetComponent<Text>().text = "プレイヤーは";
            resultText[1].GetComponent<Text>().text = "ダンジョンの" + "B" + TitleDirector.floor_ + "階にて";
            resultText[2].GetComponent<Text>().text = "トラップにかかってしまった";
            resultText[3].GetComponent<Text>().text = "Lv." + TitleDirector.levelSave_ + "総経験値:" + TitleDirector.allEx_;
        }
        else if (result == Result_Contents.TurnOver)
        {
            resultText[0].GetComponent<Text>().text = "プレイヤーは";
            resultText[1].GetComponent<Text>().text = "ダンジョンの" + "B" + TitleDirector.floor_ + "階にて";
            resultText[2].GetComponent<Text>().text = "突風にさらわれてしまった";
            resultText[3].GetComponent<Text>().text = "Lv." + TitleDirector.levelSave_ + "総経験値:" + TitleDirector.allEx_;
        }
        else if (result == Result_Contents.Clear)
        {
            resultText[0].GetComponent<Text>().text = "プレイヤーは";
            resultText[1].GetComponent<Text>().text = "ダンジョンを" + "B" + (TitleDirector.floor_) + "階まで進み";
            resultText[2].GetComponent<Text>().text = "ダンジョンを踏破した";
            resultText[3].GetComponent<Text>().text = "Lv." + TitleDirector.levelSave_ + "総経験値:" + TitleDirector.allEx_;
        }

        if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.Z))
        {

            //子の中身もしかしたらいらないかも
            {
#if UNITY_EDITOR
                UnityEditor.EditorApplication.isPlaying = false;
#else
    Application.Quit();
#endif

            }
        }
    }

}
