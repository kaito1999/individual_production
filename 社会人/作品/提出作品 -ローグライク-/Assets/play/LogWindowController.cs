﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LogWindowController : MonoBehaviour
{

    float time_ = 0.0f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        this.time_ += Time.deltaTime;

        if(time_ > 3)
        {
            time_ = 0;
            this.gameObject.SetActive(false);
        }
    }

    public void ShowLogWindow()
    {
        this.gameObject.SetActive(true);
        time_ = 0;
    }
}
