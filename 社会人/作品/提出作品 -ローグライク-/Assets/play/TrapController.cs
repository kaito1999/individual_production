﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;     //UIを使用可能にする

public class TrapController : MonoBehaviour
{
    //public bool actuationFlag_ = false;

    public PlayerController playerController_;

    private int count_;

    private LogController log_;         // ログオブジェクト

    // Start is called before the first frame update
    void Start()
    {
        log_ = GameObject.Find("LogController").GetComponent<LogController>();


        count_ = 0;

        GameObject obj = GameObject.Find("animCharacter");
        playerController_ = obj.GetComponent<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {


        if (transform.position.y > 2 || transform.position.y < -1)
        {
            Destroy(gameObject);
        }
        
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            if (playerController_ != null)
            {
                log_.LogMessage(LogController.LogList.trap, (int)(TitleDirector.hpMax_ / 10));
               // playerController_.status_.hp -= (int)(TitleDirector.hpMax_ / 10);

                //SetHPにプレイヤーの最大HPの1/10を渡して減らしてもらう
                playerController_.SetHP(TitleDirector.hpMax_ / 10, this.tag);
            }
            //actuationFlag_ = true;
            if (count_ == 0)
            {
                gameObject.GetComponent<Renderer>().material.color += new Color(0, 0, 0, 1);
                count_ = 1;
            }
            Debug.Log("actuationFlag_はtrueになったよ");
        }
    }
}
