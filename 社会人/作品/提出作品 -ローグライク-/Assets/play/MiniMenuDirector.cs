﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class MiniMenuDirector : MonoBehaviour
{
    public GameObject arrow_;

    private bool arrowFlag_;

   
    // Start is called before the first frame update
    void Start()
    {

        arrowFlag_ = true;

        this.gameObject.SetActive(false);

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow) && arrowFlag_ == false)
        {
            this.arrow_.transform.position += new Vector3(0,30,0);
            arrowFlag_ = true;
        }
        else if(Input.GetKeyDown(KeyCode.DownArrow) && arrowFlag_ == true)
        {
            this.arrow_.transform.position += new Vector3(0,-30,0);
            arrowFlag_ = false;
        }
    }
}
