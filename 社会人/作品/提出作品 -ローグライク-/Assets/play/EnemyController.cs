﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemyController : MonoBehaviour
{
    // GameObject decision; //decisionそのものが入る変数
    // GimmickGenerator script; //Select_Decisionが入る変数
    GameObject[] tagObjects;
    

   // public int count_;


    //敵ステータス
    struct Status
    {
        public int HP;   //HP
        public int ATK;  //攻撃力
        public int EXP;  //経験値
        
    }
   
    // 変数
    Status status_;

    private int layerMask;  // Rayに衝突するレイヤー

    // 定数
    const float MOVE_WIDTH = 1.0f;
    const int DEFAULT_HP = 10;
    const int DEFAULT_ATK = 2;
    const int DEFAULT_EXP = 3;

    // ポジション
    Vector3 prePoz, prePoz2, prePoz3;  //前回の位置を覚えておく
    Vector3 targetPoz;                 //移動の目的地ポジション

    // フラグ
    bool turnFlg_;      // true : プレイヤ, false : 敵
    bool deathFlg_;     // true : 死ぬ, false : 死なない
    bool onTile_;       // true : 床に乗った

    // オブジェクト
    public EnemyGenerator generator_;
    public Slider slider_;      // HPバーのスクリプト
    PlayerController Objectpoz_;
    EnemyAttack enemyAtk_;
    Collider player_;

    // Start is called before the first frame update
    void Start()
    {
        status_ = new Status { HP = DEFAULT_HP, ATK = DEFAULT_ATK, EXP = DEFAULT_EXP };

        turnFlg_ = true;
        deathFlg_ = false;
        onTile_ = false;

        // ゲームオブジェクトの初期化
        player_ = null;
//        slider_ = GameObject.Find("Slider").GetComponent<Slider>();
        generator_ = GameObject.Find("EnemyGenerator").GetComponent<EnemyGenerator>();
        Objectpoz_ = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        enemyAtk_ = transform.GetChild(0).gameObject.GetComponent<EnemyAttack>();

        // HPバー設定
        slider_.maxValue = DEFAULT_HP;
        slider_.value = DEFAULT_HP;

        layerMask = LayerMask.GetMask(new string[] { "Enemy", "wallPrefab3" });

        transform.Translate(0, 0, MOVE_WIDTH/2);

        prePoz = new Vector3(transform.position.x, 1.2f, transform.position.z);
        prePoz2 = prePoz;
        prePoz3 = prePoz;

        //オブジェクトの名前から取得して変数に格納する
        //  decision = GameObject.Find("enemyPrefab");
        //取得して変数に格納する
        //  script = decision.GetComponent<GimmickGenerator>();
    }


    void Update()
    {
        if (transform.position.y < -1 || 2 < transform.position.y)
        {
            TurnEnd();
            Destroy(this.gameObject);
        }
        if (onTile_)
        {
            //関数を入れる
            // count_ = script.enemyCount_;

            if (deathFlg_)
            {
                TurnEnd();
                EnemyDeath();
            }

            if (turnFlg_)
            {
                player_ = null;
            }
            else
            {
                                                //                enemyAtk_.StartAttack();
                player_ = enemyAtk_.GetTarget();

                // プレイヤとの距離で追跡
                if (player_ != null)
                {
                    Debug.Log("プレイヤに攻撃");
                    Attack(player_);
                }
                else if (
                  (Mathf.Abs(Objectpoz_.transform.position.x - transform.position.x) <= 3.0f)
                  &&
                  (Mathf.Abs(Objectpoz_.transform.position.z - transform.position.z) <= 3.0f)
                   )
                {
                    prePoz3 = prePoz2;
                    prePoz2 = prePoz;
                    prePoz = transform.position;    //動く前の位置記憶 
                    Chase();
                }
                else
                {
                    prePoz3 = prePoz2;
                    prePoz2 = prePoz;
                    prePoz = transform.position;    //動く前の位置記憶 
                    Move();
                }
            }
        }
    }

    // 当たり判定
    void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.tag == "Tile")
        {
            onTile_ = true;
        }
    }


    //敵の動き(ランダムに移動するだけ)
    private void Move()
    {
        // true:使用済み、false:未使用
        bool[] isMove = new bool[9];
        isMove[0] = true;
        bool moveFlg = false;
        int move = 0;

        RaycastHit hit;
        Vector3 target;

        //ここで行動を決める(今は上下左右斜めの8方向)
        while (!moveFlg)
        {
//            Random.InitState(System.DateTime.Now.Millisecond);
            while (isMove[move])
            {
                move = Random.Range(1, 9);
            }
            isMove[move] = true;

            switch (move)
            {
                case 1: target = new Vector3(0, 0, MOVE_WIDTH); break;
                case 2: target = new Vector3(0, 0, -MOVE_WIDTH); break;
                case 3: target = new Vector3(MOVE_WIDTH, 0, 0); break;
                case 4: target = new Vector3(-MOVE_WIDTH, 0, 0); break;
                case 5: target = new Vector3(MOVE_WIDTH, 0, MOVE_WIDTH); break;
                case 6: target = new Vector3(MOVE_WIDTH, 0, -MOVE_WIDTH); break;
                case 7: target = new Vector3(-MOVE_WIDTH, 0, MOVE_WIDTH); break;
                case 8: target = new Vector3(-MOVE_WIDTH, 0, -MOVE_WIDTH); break;
                default: target = new Vector3(0, 0, 0); break;
            }

            if (Physics.Linecast(transform.position, transform.position + target, out hit, layerMask))
            {
                moveFlg = true;
                // 未調査があるならもう一周
                foreach (bool confirm in isMove)
                {
                    if (!confirm)
                    {
                        moveFlg = false;
                    }
                }
            }
            else
            {
                transform.Translate(target);
                TurnEnd();
                moveFlg = true;
            }

        }
    }
    
    // 攻撃
    private void Attack(Collider collider)
    {
        PlayerController conpornent = collider.GetComponent<PlayerController>();

        conpornent.SetHP(status_.ATK, this.tag);
        TurnEnd();
    }

    // 追跡
    private void Chase()
    {
        RaycastHit hit;
        Vector3 horTarget = Vector3.zero;
        Vector3 depTarget = Vector3.zero;

        bool hor = false;
        bool dep = false;

        if ((int)Objectpoz_.transform.position.x < (int)transform.position.x)
        {
            horTarget = new Vector3(-MOVE_WIDTH, 0, 0);
            hor = true;
        }
        else if ((int)Objectpoz_.transform.position.x > (int)transform.position.x)
        {
            horTarget = new Vector3(MOVE_WIDTH, 0, 0);
            hor = true;
        }
        if ((int)Objectpoz_.transform.position.z < (int)transform.position.z)
        {
            depTarget = new Vector3(0, 0, -MOVE_WIDTH);
            dep = true;
        }
        else if ((int)Objectpoz_.transform.position.z > (int)transform.position.z)
        {
            depTarget = new Vector3(0, 0, MOVE_WIDTH);
            dep = true;
        }

        if (Physics.Linecast(transform.position, transform.position + horTarget, out hit, layerMask))
        {
            hor = false;
        }
        if (Physics.Linecast(transform.position, transform.position + depTarget, out hit, layerMask))
        {
            dep = false;
        }


        if (dep && hor)
        {
            int choice = Random.Range(0, 1);
            if (choice == 0)
            {
                transform.Translate(depTarget);
            }
            else
            {
                transform.Translate(horTarget);
            }
        }
        else if (dep)
        {
            transform.Translate(depTarget);
        }
        else if (hor)
        {
            transform.Translate(horTarget);
        }

        TurnEnd();
    }

    void Check(string tagname)
    {
        tagObjects = GameObject.FindGameObjectsWithTag(tagname);
    }
    
    // 巻き戻しを受ける
    public void Regain()
    {
        transform.position = prePoz;
        prePoz = prePoz2;
        prePoz2 = prePoz3;
    }
    // 敵のターン終了
    public void TurnEnd()
    {
        generator_.EnemyTurnEnd();
       // enemyAtk_.EndAttack();
        turnFlg_ = true;
    }
    // 死ぬ処理
    public void EnemyDeath()
    {
        Objectpoz_.SetEX(status_.EXP, this.tag);
        Destroy(this.gameObject);
    }

    // -----------セッター / ゲッター------------
    // ターンをセット false:敵ターン、true:プレイヤターン
    public void SetTurn(bool turn)
    {
        turnFlg_ = turn;
//        turnFlg_ = generator_.GetTurn();
    }
    // ダメージを受ける
    public void SetHP(int damage)
    {
        status_.HP -= damage;
        slider_.value = status_.HP;
        if (status_.HP <= 0)
        {
            deathFlg_ = true;
        }
    }
}
