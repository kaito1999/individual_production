﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapGenerator : MonoBehaviour
{

    public GameObject trapPrefab;

    // Start is called before the first frame update
    void Start()
    {
        trapGenerator();
    }

    // Update is called once per frame
    void Update()
    {
      
    }

    private void trapGenerator()
    {
        for(int i = 0 ; i < 3 ; i++)
        {
            GameObject Trap_ = Instantiate(trapPrefab) as GameObject;

            //ランダムで出現させる
            int SpawnX_ = Random.Range(18, -20);
            int SpawnY_ = Random.Range(18, -20);

            Trap_.transform.position = new Vector3(SpawnX_ + 0.5f, 2, SpawnY_ + 0.5f);
        }
    }


}
