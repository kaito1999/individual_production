﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoadDelete : MonoBehaviour
{

    public PlayerController playerController_;

    // Start is called before the first frame update
    void Start()
    {
        GameObject obj = GameObject.Find("animCharacter");
        playerController_ = obj.GetComponent<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {
        bool flagCheck_;

        flagCheck_ = playerController_.loadFlag_;

        if (Input.GetKeyDown(KeyCode.A) || flagCheck_ == true)
        {
            Destroy(gameObject);
        }
    }
}
