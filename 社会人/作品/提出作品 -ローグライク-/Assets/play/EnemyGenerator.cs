﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGenerator : MonoBehaviour
{
    //タグの数を数えるときに使う
    GameObject[] tagObjects;
    // 敵プレハブを格納するリスト
    List<GameObject> enemyList_ = new List<GameObject>();


    //   GameObject decisionEnemy_; //decisionそのものが入る変数
    //   EnemyController scriptEnemy_; //Select_Decisionが入る変数

    public GameObject enemyPrefab;

    // プレイヤーのスクリプト
    public PlayerController player_;
    // マップジェネレータのスクリプト
    public MapGenerator mapGenerator_;

    //現在の敵の数
    public int enemyCount_ = 0;
    //初期の数の敵を全部出し終わったら起動させる
    public bool startFlag_ = false;
    //敵の最大数
    const int ENEMY_MAX = 8;
    //ランダムの時に使う
    public int Rand;

    // ture : プレイヤー, false : 敵
    public bool turnFlg_;
    // 敵が全員ターンを終えたかのカウント
    public int enemyTurnCount_;


    void Start()
    {
        enemyTurnCount_ = 0;
        turnFlg_ = true;
        // 引数 : プレイヤのオブジェクト名
        player_ = GameObject.Find("animCharacter").GetComponent<PlayerController>();
        mapGenerator_ = GameObject.Find("MapGenerator").GetComponent<MapGenerator>();

        //オブジェクトの名前から取得して変数に格納する
        //    decisionEnemy_ = GameObject.Find("enemyPrefab");
        //取得して変数に格納する
        //   scriptEnemy_ = decisionEnemy_.GetComponent<EnemyController>();

    }

    void Update()
    {
        //関数を入れる
        //    enemyCount_ = scriptEnemy_.count_;

        EnemyGenerate();
        //if(player_.turnFlg_)

        //prefab名ではなくtagを入力する
        if (startFlag_ == true)
        {
            Check("Enemy");
        }
    }


    private void EnemyGenerate()
    {

        if (Input.GetKeyDown(KeyCode.Space))
        {
            if (ENEMY_MAX == tagObjects.Length)
            {
                Rand = 9999;
            }
            else
            {
                //ランダムで0～9まで出して変数に入れる
                Rand = Random.Range(0, 9);
                Debug.Log(Rand); //tagObjects.Lengthはオブジェクトの数

            }

        }

        //敵を出現させる
        //敵が減ったら増やす
        if (startFlag_ == false || startFlag_ == true && tagObjects.Length <= 3|| enemyCount_ < ENEMY_MAX && Input.GetKeyDown(KeyCode.Space))
        {
            GameObject enemy2_ = Instantiate(enemyPrefab) as GameObject;

            if(startFlag_)
            {
                // マップジェネレータからランダムなタイルを取得
                enemy2_.transform.position = mapGenerator_.GetRandomTile();
                // ↑はタイルの角なので真ん中に移動させる
                enemy2_.transform.Translate(-0.5f, 2, 0);
            }
            else
            {
                //ランダムで出現させる
                int SpawnX_ = Random.Range(18, -20);
                int SpawnY_ = Random.Range(18, -20);

                //出す場所を決める
                enemy2_.transform.position = new Vector3(SpawnX_+0.5f, 2, SpawnY_);

            }



            //カウントを増やして今現在の敵の数を数える
            enemyCount_++;

            if (enemyCount_ == ENEMY_MAX)
            {
                startFlag_ = true;
            }
        }
    }

    //シーン上のBlockタグが付いたオブジェクトを数える
    void Check(string tagname)
    {
        tagObjects = GameObject.FindGameObjectsWithTag(tagname);

        enemyList_.Clear();
        foreach(GameObject chara in tagObjects)
        {
            enemyList_.Add(chara);
        }

        //現在のタグ付けされたEnemyの数が最大値より少ないときかつ40%の確率で出せるようにした
        if (ENEMY_MAX > tagObjects.Length && Rand <= 3)
        {
            enemyCount_ = tagObjects.Length;
        }


    }

    //-------------- ターン関係 -----------------
    // 敵が全員ターン終了したか
    public void EnemyTurnEnd()
    {
        // ごり押し
//        ChangeTurn(true);
        //enemy_.SetTurn(true);

        enemyTurnCount_++;
        //tagObjects = GameObject.FindGameObjectsWithTag("Enemy");
        if (tagObjects != null)
        {
            if (enemyTurnCount_ >= tagObjects.Length)
            {
                // 全員のターンが終了したらプレイヤーターンに
                ChangeTurn(true);
                enemyTurnCount_ = 0;
            }
        }
        
    }
    // ターンを切り替える
    public void ChangeTurn(bool turn)
    {
        // ターンフラグをキャラクターが受け取る式
        turnFlg_ = turn;

        if(turn)
        {
            player_.SetFlg(turn);            
        }
        else
        {
            foreach(GameObject chara in enemyList_)
            {
                if (chara != null)
                {
                    chara.GetComponent<EnemyController>().SetTurn(turn);
                }
            }
        }
    }   

    // 敵の巻き戻しを使う関数。プレイヤから直接、簡潔に呼べると最高だね
    public void EnemyRegain()
    {
        foreach(GameObject chara in enemyList_)
        {
            chara.GetComponent<EnemyController>().Regain();
        }
    }


    // ------------セッター/ゲッター--------------------
    public bool GetTurn()
    {
        return turnFlg_;
    }
    public void SetTurn(bool turn)
    {
        turnFlg_ = turn;
    }


}


