﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System;
using System.Threading;

// ステータス
public struct STATUS
{
    public int level;       // レベル
    public int experience;  // 経験値
    public int hp;          // 体力
    public int atk;         // 攻撃力
    public int regain;      // 巻き戻し回数
}

// 振り向きの角度
enum ROTATE
{
    UP = 0,
    UP_RIGHT = 45,
    RIGHT = 90,
    DOWN_RIGHT = 135,
    DOWN = 180,
    DOWN_LEFT = 225,
    LEFT = 270,
    UP_LEFT = 315
}


public class PlayerController : MonoBehaviour
{
    // 攻撃用コライダーに適用するレイヤー番号
    private enum LAYER_NUMBER
    {
        ATTACK = 14,
        ESCAPE = 31
    }
    private enum MOVE_TYPE
    {
        WALK,       // 移動
        WALK_CHECK, // 移動が可能か調べる
        ATTACK,     // 攻撃
        BAG,        // アイテムリスト
        REGAIN,     // 巻き戻し
        MAX         // 初期値
    }

    public  STATUS status_;             // ステータス

    // フラグ
    private bool waitFlg_;      // true:通常フロー、false:処理を止める
    private bool attackFlg_;    // 攻撃が当たるかどうか
    private bool moveFlg_;      // true:移動した、false:移動していない           
    private bool itemSubmenu_;  // サブメニュー　true:表示中、false:非表示
    private bool selectItemSub_;// サブメニューのyes/no　true:yes、no:true
    private bool stairOut_;     // 階段から出たか　true:出た、false:まだいる

    public bool turnFlg_;       // true:自ターン、false:敵ターン
    public bool loadFlag_;      // ロード画面を消すためのフラグ
    public bool reFlag_;        // なにフラグ？
    public bool resultFlg_;     // true:リザルト中, false:リザルトじゃない


    // 定数
    const float STRIDE = 1.0f;          // 一歩で動く距離
    const float SPEED = 5.0f;           // 移動スピード
    const float RECOVERY_DIV = 150f;    // 回復量の計算
    const float ATTACK_SPEED = 1.5f;    // 攻撃モーションの速度
    const float ATTACK_SECOND = 0.5f;   // 攻撃にかかる時間
    const int RAY_DIS = 2;              // レイを飛ばす距離
    const int EX_UP = 5;                // レベル上昇時の経験値上限を上げる値
    const int ATK_UP = 2;               // レベル上昇時の攻撃力上昇値
    const int HP_UP = 2;                // レベル上昇時の体力上昇値
    const int REGAIN = 3;               // 巻き戻し回数

    const int TIME_UP = 1000;

    const int HOR_ARROW = 300;          // アイテムリスト矢印のx移動値
    const int APPLE_HEAL = 10;          // 小リンゴの回復値
    const int BIG_APPLE_HEAL = 20;      // 大リンゴの回復値

    // 変数
    private int surplusEx_;     // レベル上昇後に余っている経験値を格納する
    private int itemNum_;       // アイテムリストの添え字
    private int moveLayerMask;  // 移動時にRayと衝突するレイヤー
    private int attackLayerMask;// 攻撃時にRayと衝突するレイヤー

    private float recovery_;    // 回復量。1を超えると実質の回復
    private float time;         // 攻撃に使う時間変数

    private UIManager.Result_Contents resContent_;

    int turnDeathCount;
    private bool hierarchyFlag_;

    private string resultTag_;  // 自分を倒した敵のタグ

    Vector3 tarPosition;        // 目的地のポジション
    Vector3 prePosition;        // 前のポジション
    Vector3 variableCase_;      // このクラス内での代わりの変数

    private Collider enemy_;    // 攻撃時に使うコライダー変数

    private MOVE_TYPE moveType_;// enumの値を入れる変数
    private KeyCode code_;      // 押されたキーを入れる

    // ゲームオブジェクト
    public MapGenerator mapGenerator_;  // マップジェネレータオブジェクト
    public EnemyGenerator generator_;   // ジェネレータオブジェクト
    private PlayerAttack playerAtk_;    // 攻撃当たり判定のオブジェクト
    private LogController log_;         // ログオブジェクト
    private UIManager uiManage_;        // UIオブジェクト

    // ほんとはアローではない
    public GameObject arrow;

    public GameObject itemCanvas_;      // アイテムキャンバスオブジェクト
    public GameObject itemContent_;     // キャンバス下のコンテンツオブジェクト
    public GameObject item_;            // アイテム

    public GameObject itemArrow;      // 本当にアロー
    public GameObject submenu_;         // サブメニューオブジェクト

    private GameObject content_;        // instantiate用

    // リスト
    private List<ItemCorrection.ITEM_ID> playerItemList_;
    private List<GameObject> itemTextList_;
   
    private int steppingOn_;

    public AudioClip soundMenuOpen_;    //メニューが開かれたときの音を鳴らす
    public AudioClip soundMenuSelect_;  //メニューの選択肢を切り替えたときの音を鳴らす
    public AudioClip soundMenuEnter_;   //メニューの選択肢を選んだ時に音を鳴らす
    public AudioClip soundMenuCancel_;  //メニューの選択肢をキャンセルした時に音を鳴らす
    public AudioClip getItem_;          //アイテムを入手した時に音を鳴らす
    public AudioClip attackSe_;         //攻撃した時に音を鳴らす
    public AudioClip regainSe_;         //巻き戻し機能を使ったときに音を鳴らす
    public AudioClip trapSe_;           //トラップを踏んだ時に鳴らす音
    public AudioClip enemyDamageSe_;    //敵からダメージをもらったときにに鳴らす音
    public AudioClip levelUpSe_;        //レベルが上がったときに鳴らす音楽

    AudioSource audioSource;

    private bool countFlag_;

    private int seCount_;

    void Start()
    {
        // リストの初期化
        itemTextList_ = new List<GameObject>();
        playerItemList_ = new List<ItemCorrection.ITEM_ID>(ItemCorrection.itemList);

        seCount_ = 0;

        ///
        audioSource = GetComponent<AudioSource>();

        countFlag_ = false;
        ///

        // 変数の初期化
        steppingOn_ = 0;
        recovery_ = 0;
        time = 0.0f;
        enemy_ = null;
        resultTag_ = null;
        moveType_ = MOVE_TYPE.MAX;
        turnDeathCount = 0;

        itemNum_ = 0;
        moveLayerMask = LayerMask.GetMask(new string[] { "Enemy", "wallPrefab3" });
        attackLayerMask = LayerMask.GetMask(new string[] { "Enemy" });
        // 構造体初期化
        status_ = new STATUS
        {
            level = TitleDirector.levelSave_,
            experience = TitleDirector.exSave_,
            hp = TitleDirector.hpSave_,
            atk = TitleDirector.atkSave_,
            regain = 3
        };

        // 目的地、オブジェクト初期化
//        tarPosition = transform.position;

        mapGenerator_ = GameObject.Find("MapGenerator").GetComponent<MapGenerator>();
        generator_ = GameObject.Find("EnemyGenerator").GetComponent<EnemyGenerator>();
        log_ = GameObject.Find("LogController").GetComponent<LogController>();
        uiManage_ = GameObject.Find("PlayerUIManager").GetComponent<UIManager>();
        playerAtk_ = transform.GetChild(4).gameObject.GetComponent<PlayerAttack>();





        // フラグ初期化
        turnFlg_ = true;
        waitFlg_ = true;
        stairOut_ = true;
        hierarchyFlag_ = true;

        loadFlag_ = false;
        moveFlg_ = false;
        reFlag_ = false;
        resultFlg_ = false;
        itemSubmenu_ = false;
        selectItemSub_ = true;

       

    content_ = (GameObject)Instantiate(itemContent_, itemCanvas_.transform.GetChild(0).transform);
        GetComponent<Rigidbody>().useGravity = false;
    }

    void Update()
    {
        
        // プレイヤーのリスポーン地点
        if (mapGenerator_.respawnFlag_)
        {
            // マップにランダムなタイルを持って来てもらう
            Vector3 respawnPos = 
                mapGenerator_.GetRandomTile() + new Vector3(-0.5f, 2.0f, 0.5f);
            transform.position = respawnPos;

            //transform.position = mapGenerator_.GetRandomTile();
            //transform.Translate(-0.5f, 2.0f, 0.5f);

            // 謎の回転があったので固定
            transform.rotation = Quaternion.Euler(new Vector3(0.0f, 0.0f, 0.0f));
            tarPosition = transform.position;

            // 重力を切って移動させていたので、ここで重力を付ける
            GetComponent<Rigidbody>().useGravity = true;
            mapGenerator_.respawnFlag_ = false;
        }


        // リザルト表示
        if (resultFlg_)
        {
            uiManage_.ResultScreen(resContent_);
        }
        // 通常処理
        else
        {
            // 地面に居て、自分のターンなら
            if (loadFlag_ && turnFlg_)
            {
                // 1F 攻撃用コライダー起動 + コマンド系
                if (playerAtk_.GetLayer() == (int)LAYER_NUMBER.ESCAPE)
                {
                    // 階段に触れていないなら
                    if (waitFlg_)
                    {
                        
                        if (Input.GetKey(KeyCode.LeftShift))
                        {
                            // 向きを変える操作
                            ChangeDir();
                        }
                        else
                        {
                            // その他操作のコマンドを受け取る
                            CommandSelect();
                        }
                    }
                    else
                    {
                        // 階段に触れた時の処理
                        StairProcess();
                    }
                }
                // 2frame 行動を適用する
                else if (playerAtk_.GetLayer() == (int)LAYER_NUMBER.ATTACK)
                {
                    // 受け取ったコマンドから行動を適用する
                    SwitchAct();
                }
            }

            // 壁に生まれたら死にたい
            if (transform.position.y > 2 || transform.position.y < -1)
            {
                //シーン読み込み
                SceneManager.LoadScene("PlayScene"); 
                Destroy(gameObject);
            }
        }

      
    }


    // --------------------------- コリジョン同士の当たり判定 ---------------------------
    void OnCollisionEnter(Collision collision)
    {
        // 階段なら
        if (collision.gameObject.tag == "Stairs")
        {
            audioSource.PlayOneShot(soundMenuSelect_);
            if (stairOut_)
            {
                // 処理を止めて、階層を上るか選択する画面を出すフラグ
                // stairFlgは要らないかもs
                arrow.SetActive(true);
                waitFlg_ = false;
                stairOut_ = false;
            }
        }

        // 地面タイルなら
        if (collision.gameObject.tag == "Tile" && !loadFlag_ && !mapGenerator_.respawnFlag_)
        {
            Debug.Log("地面に着いた！");

            // 地面に触れているかチェック
            loadFlag_ = true;

            tarPosition = transform.position;
//            transform.rotation = Quaternion.Euler(new Vector3(0.0f, 0.0f, 0.0f));
        }

        // アイテムなら
        if (collision.gameObject.tag == "Item")
        {
            playerItemList_.Add(ItemCorrection.NameToID(collision.gameObject.name));

            // アイテムのファイル名からIDへ、IDからアイテム名へ
            string itemName = ItemCorrection.IDToName(ItemCorrection.NameToID(collision.gameObject.name));

            // アイテム名をログに書く
            log_.LogMessage(LogController.LogList.item, 0, null, itemName);
            Destroy(collision.gameObject);

            audioSource.PlayOneShot(getItem_);
        }

    }


    // ----------------------方向転換----------------------
    private void ChangeDir()   
    {
        if(Input.GetKey(KeyCode.LeftShift))
        {
            

            // 8方向に時計回りで記述
            // 前を向く
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                transform.rotation = Quaternion.Euler(0, (float)ROTATE.UP, 0);
            }
            // 右前を向く
            if (Input.GetKey(KeyCode.UpArrow) && Input.GetKey(KeyCode.RightArrow))
            {
                transform.rotation = Quaternion.Euler(0, (float)ROTATE.UP_RIGHT, 0);
            }
            // 右を向く
            if(Input.GetKeyDown(KeyCode.RightArrow))
            {
                transform.rotation = Quaternion.Euler(0, (float)ROTATE.RIGHT, 0);
            }
            //右後ろを向く
            if(Input.GetKey(KeyCode.DownArrow) && Input.GetKey(KeyCode.RightArrow))
            {
                transform.rotation = Quaternion.Euler(0, (float)ROTATE.DOWN_RIGHT, 0);
            }
            // 後ろを向く
            if(Input.GetKeyDown(KeyCode.DownArrow))
            {
                transform.rotation = Quaternion.Euler(0, (float)ROTATE.DOWN, 0);
            }
            // 左後ろを向く
            if(Input.GetKey(KeyCode.DownArrow) && Input.GetKey(KeyCode.LeftArrow))
            {
                transform.rotation = Quaternion.Euler(0, (float)ROTATE.DOWN_LEFT, 0);
            }
            // 左を向く
            if(Input.GetKeyDown(KeyCode.LeftArrow))
            {
                transform.rotation = Quaternion.Euler(0, (float)ROTATE.LEFT, 0);
            }
            // 左前を向く
            if(Input.GetKey(KeyCode.UpArrow) && Input.GetKey(KeyCode.LeftArrow))
            {
                transform.rotation = Quaternion.Euler(0, (float)ROTATE.UP_LEFT, 0);
            }

        }
    }

    // コマンドを受け取り、適用する行動を選択
    private void CommandSelect()
    {
        // 斜め移動
        if (Input.GetKey(KeyCode.LeftControl))
        {
            // 右前に移動
            if (Input.GetKey(KeyCode.UpArrow) && Input.GetKey(KeyCode.RightArrow))
            {
                tarPosition =
                    new Vector3(transform.position.x + STRIDE, transform.position.y, transform.position.z + STRIDE);
            }
            // 右後ろに移動
            else if (Input.GetKey(KeyCode.RightArrow) && Input.GetKey(KeyCode.DownArrow))
            {
                tarPosition =
                    new Vector3(transform.position.x + STRIDE, transform.position.y, transform.position.z - STRIDE);
            }
            // 左後ろに移動
            else if (Input.GetKey(KeyCode.LeftArrow) && Input.GetKey(KeyCode.DownArrow))
            {
                tarPosition =
                    new Vector3(transform.position.x - STRIDE, transform.position.y, transform.position.z - STRIDE);
            }
            // 左前に移動
            else if (Input.GetKey(KeyCode.UpArrow) && Input.GetKey(KeyCode.LeftArrow))
            {
                tarPosition =
                    new Vector3(transform.position.x - STRIDE, transform.position.y, transform.position.z + STRIDE);
            }
        }
        else
        {

            // 十字移動
            // 前に移動
            if (Input.GetKey(KeyCode.UpArrow))
            {
                tarPosition =
                    new Vector3(transform.position.x, transform.position.y, transform.position.z + STRIDE);
            }
            // 後ろに移動
            else if (Input.GetKey(KeyCode.DownArrow))
            {
                tarPosition =
                  new Vector3(transform.position.x, transform.position.y, transform.position.z - STRIDE);
            }
            // 左に移動
            else if (Input.GetKey(KeyCode.LeftArrow))
            {
                tarPosition =
                    new Vector3(transform.position.x - STRIDE, transform.position.y, transform.position.z);
            }
            // 右に移動
            else if (Input.GetKey(KeyCode.RightArrow))
            {
                tarPosition =
                    new Vector3(transform.position.x + STRIDE, transform.position.y, transform.position.z);
            }
            // 攻撃
            else if ((Input.GetKeyDown(KeyCode.Z)) ||(Input.GetKeyDown(KeyCode.Return)))
            {
                //攻撃した時に音を鳴らす
                audioSource.PlayOneShot(attackSe_);

                moveType_ = MOVE_TYPE.ATTACK;
                playerAtk_.SetLayer((int)LAYER_NUMBER.ATTACK);

                // 最初に攻撃できるか当たり判定をチェック
                enemy_ = playerAtk_.GetTarget();

                // 攻撃が敵に当たらなかったら
                if(enemy_.gameObject.tag != "Enemy")
                {
                    log_.LogMessage(LogController.LogList.atkMiss_);
                }
            }
            // アイテムリスト表示
            else if (Input.GetKeyDown(KeyCode.B))
            {
                audioSource.PlayOneShot(soundMenuOpen_);
                itemNum_ = 0;


                // バッグ関数へ移行
                moveType_ = MOVE_TYPE.BAG;
                playerAtk_.SetLayer((int)LAYER_NUMBER.ATTACK);

                // スクロールビューを召喚
                itemCanvas_.SetActive(true);

                

                // テキストを召喚
                if (playerItemList_.Count > itemTextList_.Count)
                {
                    for (int index = itemTextList_.Count; index < playerItemList_.Count; index++)
                    {
                        item_.transform.GetChild(0).GetComponent<Text>().text =
                            ItemCorrection.IDToName(playerItemList_[index]);
                        itemTextList_.Add(Instantiate(item_, content_.transform));
                    }
                }


                // 矢印の初期は一番上を選択
                if (playerItemList_.Count != 0)
                {                    
                    itemArrow.SetActive(true);
                    itemArrow.transform.position = itemTextList_[(int)itemNum_].transform.position;
                    itemArrow.transform.Translate(HOR_ARROW, 0, 0);
                }
                else if(playerItemList_.Count == 0)
                {
                    // アイテムが無いなら矢印を出さない
                    itemArrow.SetActive(false);
                }


            }
            // 巻き戻し
            else if (Input.GetKeyDown(KeyCode.V) && (status_.regain > 0))
            {

                audioSource.PlayOneShot(regainSe_);
                // 次のフレームで巻き戻し関数へ移動
                moveType_ = MOVE_TYPE.REGAIN;
                playerAtk_.SetLayer((int)LAYER_NUMBER.ATTACK);
                
            }
            // 足踏み
            else if (Input.GetKey(KeyCode.A))
            {
                steppingOn_++;

                if (steppingOn_ % 10 == 0)
                {
                    // 見た目的には何して欲しいですか？？
                    TurnEnd();
                }
            }
        }

        tarPosition.y = transform.position.y;

        // 移動する場合は壁があるか確認
        if (moveFlg_)
        {
            moveType_ = MOVE_TYPE.WALK_CHECK;
            playerAtk_.SetLayer((int)LAYER_NUMBER.ATTACK);
        }
        // 動いた向きに合わせて回転
        Vector3 diff = tarPosition - transform.position;
        if (diff.magnitude > 0.01f)
        {
            transform.rotation = Quaternion.LookRotation(diff);
            moveFlg_ = true;

        }

    }

    // 選択された行動を実行
    private void SwitchAct()
    {
        switch (moveType_)
        {
            case MOVE_TYPE.WALK_CHECK:
                MoveCheck();
                break;

            case MOVE_TYPE.WALK:
                Move();
                break;

            case MOVE_TYPE.ATTACK:
                Attack();
                break;

            case MOVE_TYPE.BAG:
                
                Bag();
                break;

            case MOVE_TYPE.REGAIN:
                Regain();
                break;

            default:
                playerAtk_.SetLayer((int)LAYER_NUMBER.ESCAPE);
                break;
        }
    }

    // 壁や敵など移動の妨げになるものが無いか確認
    private void MoveCheck()
    {
        RaycastHit hit;
        if (Physics.Linecast(transform.position, tarPosition, out hit, moveLayerMask))
        {
            tarPosition = transform.position;

            moveFlg_ = false;
            moveType_ = MOVE_TYPE.MAX;
            playerAtk_.SetLayer((int)LAYER_NUMBER.ESCAPE);
        }
        // 敵と壁に当たらなければ
        else
        {
            moveType_ = MOVE_TYPE.WALK;
            tarPosition = new Vector3(tarPosition.x, transform.position.y, tarPosition.z);
        }
      
    }

    // 移動を適用する
    private void Move()
    {
        transform.position = Vector3.MoveTowards(transform.position, tarPosition, SPEED * Time.deltaTime);

        // yの移動ではターン終了しないように
        if ((transform.position.x == tarPosition.x) && (transform.position.z == tarPosition.z))
        {
            moveFlg_ = false;
            TurnEnd();

            if (waitFlg_)
            {
                stairOut_ = true;
            }

        }

        // 転ばないように強制する
        transform.rotation = new Quaternion(0, transform.rotation.y, 0, transform.rotation.w);
    }

    // -------------------- 単発行動をする用の関数たち --------------------
    // 攻撃をする
    private void Attack()
    {


        time += Time.deltaTime;
        float seconds = Mathf.Sin(time * 3.14f / ATTACK_SECOND);  // 移動量

        if (ATTACK_SECOND <= time)
        {
            // 攻撃モーションが終わったらダメージを与えてターン終了
            if (enemy_ != null)
            {
         
                if (enemy_.gameObject.tag == "Enemy")
                {
                    enemy_.GetComponent<EnemyController>().SetHP(status_.atk);
                    log_.LogMessage(LogController.LogList.attack, status_.atk, enemy_.gameObject.tag);

                }
            }
            transform.position = tarPosition;
            time = 0;
            TurnEnd();
        }
        else if ((ATTACK_SECOND / 2) <= time)
        {
            // 後半は後ろへ戻る
            transform.Translate(0, 0, -(ATTACK_SPEED * seconds / 50));
        }
        else
        {
            // 前半は前へ進む
            transform.Translate(0, 0, ATTACK_SPEED * seconds / 50);
        }
    }

    // バッグ関数
    private void Bag()
    {
       
        // サブメニューを開いている
        if (itemSubmenu_)
        {
            submenu_.SetActive(true);

            //ミニメニュー
            if (Input.GetKeyDown(KeyCode.UpArrow))
            {
                selectItemSub_ = true;
                audioSource.PlayOneShot(soundMenuSelect_);
            }
            else if (Input.GetKeyDown(KeyCode.DownArrow))
            {
                selectItemSub_ = false;
                audioSource.PlayOneShot(soundMenuSelect_);
            }

            // サブメニューで決定キーを押した場合
            if ((Input.GetKeyDown(KeyCode.Z))|| (Input.GetKeyDown(KeyCode.Return)))
            {
                
                // アイテムを使用する
                if (selectItemSub_)
                {
                    // 効果発動
                    switch (playerItemList_[itemNum_])
                    {
                        case ItemCorrection.ITEM_ID.APPLE:
                            status_.hp += APPLE_HEAL;
                            if (TitleDirector.hpMax_ < status_.hp)
                            {
                                status_.hp = TitleDirector.hpMax_;
                            }

                            log_.LogMessage(LogController.LogList.heal, APPLE_HEAL);
                            break;
                        case ItemCorrection.ITEM_ID.BIGAPPLE:
                            status_.hp += BIG_APPLE_HEAL;

                            if (TitleDirector.hpMax_ < status_.hp)
                            {
                                status_.hp = TitleDirector.hpMax_;
                            }

                            log_.LogMessage(LogController.LogList.heal, BIG_APPLE_HEAL);
                            break;
                        case ItemCorrection.ITEM_ID.MAX:
                            break;
                    }

                    // リストから削除。
                    playerItemList_.RemoveAt(itemNum_);
                    // 1つ空きにならないように全削除
                    itemTextList_.Clear();
                    foreach (Transform child in content_.transform)
                    {
                        Destroy(child.gameObject);
                        audioSource.PlayOneShot(soundMenuEnter_);
                    }


                    //サブメニューを閉じる
                    itemSubmenu_ = false;
                    submenu_.SetActive(false);

                    // バッグを閉じる
                    itemCanvas_.SetActive(false);
                    playerAtk_.SetLayer((int)LAYER_NUMBER.ESCAPE);
                    moveType_ = MOVE_TYPE.MAX;
                }
                // アイテムを使用しない
                else
                {
                    // サブメニューを閉じる
                    itemSubmenu_ = false;
                    submenu_.SetActive(false);
                    audioSource.PlayOneShot(soundMenuCancel_);
                }
            }

        }
        // サブメニューを開いていない
        else
        {
            // アイテム選択
            if (Input.GetKeyDown(KeyCode.UpArrow) && itemNum_ != 0)
            {
                itemNum_--;
                audioSource.PlayOneShot(soundMenuSelect_);
            }
            else if (Input.GetKeyDown(KeyCode.DownArrow) && (itemNum_ < itemTextList_.Count - 1))
            {
                itemNum_++;
                audioSource.PlayOneShot(soundMenuSelect_);
            }
            // 使う(サブメニューを開く)
            else if ((Input.GetKeyDown(KeyCode.Z)||Input.GetKeyDown(KeyCode.Return)) && playerItemList_.Count != 0)
            {
                
                itemSubmenu_ = true;
                //                submenu_.SetActive(true);
                audioSource.PlayOneShot(soundMenuEnter_);

            }
            // リストを閉じる
            else if (Input.GetKeyDown(KeyCode.B))
            {
                itemCanvas_.SetActive(false);
                playerAtk_.SetLayer((int)LAYER_NUMBER.ESCAPE);
                moveType_ = MOVE_TYPE.MAX;


                audioSource.PlayOneShot(soundMenuCancel_);
                

                foreach (var i in itemTextList_)
                {
                    Debug.Log(i);
                }
               
            }



            // リストが空じゃなければ
            if (itemTextList_.Count != 0)
            {
                // 矢印の変更を適用
                itemArrow.transform.position = itemTextList_[(int)itemNum_].transform.position;
                itemArrow.transform.Translate(HOR_ARROW, 0, 0);
            }
        }
    }

    private void Regain()
    {
        generator_.EnemyRegain();
        status_.regain--;
        playerAtk_.SetLayer((int)LAYER_NUMBER.ESCAPE);
        moveType_ = MOVE_TYPE.MAX;
    }

    // 回復をする
    private void Recover()
    {
        // 体力が最大ではない時
        if (status_.hp < TitleDirector.hpMax_)
        {
            recovery_ += (TitleDirector.hpMax_ / RECOVERY_DIV);
        }

        // 回復量が1を超えたら1回復する
        if (1f <= recovery_)
        {
            status_.hp += 1;
            recovery_ -= 1.0f;
            //log_.LogMessage(LogController.LogList.heal, 1);
        }

    }

    // ターン終了時に呼ぶ
    private void TurnEnd()
    {

        //強制エンドのやつ
        {
            turnDeathCount++;
            switch (turnDeathCount)
            {
                case 250: log_.LogMessage(LogController.LogList.suddendeath1, 0, null, null); break;
                case 500: log_.LogMessage(LogController.LogList.suddendeath2, 0, null, null); break;
                case 750: log_.LogMessage(LogController.LogList.suddendeath3, 0, null, null); break;
                case 1000:
                    PlayerDeath();
                    resContent_ = UIManager.Result_Contents.TurnOver;
            
                    log_.LogMessage(LogController.LogList.suddendeath4, 0, null, null); break;
            }
        }
       
       

        Recover();
        playerAtk_.SetLayer((int)LAYER_NUMBER.ESCAPE);
        moveType_ = MOVE_TYPE.MAX;

        turnFlg_ = false;
        generator_.ChangeTurn(false);
      
    }

    // 体力が0になったら死ぬ
    public void PlayerDeath()
    {
        TitleDirector.hpSave_ = status_.hp;
        TitleDirector.levelSave_ = status_.level;

        TitleDirector.allEx_ += status_.experience;

        resultFlg_ = true;

    }

    // レベルアップ関数
    private void LevelUp()
    {

        //レベルが上がったときに鳴らす音楽
        audioSource.PlayOneShot(levelUpSe_);

        Debug.Log("レベルが上がったよ");
        status_.level++;
        log_.LogMessage(LogController.LogList.levelup, status_.level);

        TitleDirector.hpMax_ += HP_UP;
        status_.hp += HP_UP;
        log_.LogMessage(LogController.LogList.hpup, HP_UP);

        status_.atk += ATK_UP;
        log_.LogMessage(LogController.LogList.attackup, ATK_UP);

        TitleDirector.allEx_ += TitleDirector.exMax_;
        status_.experience = (status_.experience - TitleDirector.exMax_);

        TitleDirector.exMax_ += (TitleDirector.exMax_ + EX_UP);
    }

    // 階段に触れている時
    private void StairProcess()
    {

        //すまん作ってる最中に絶妙に精神持ってかれたからあとでまともなソースにする(多分)
        if (Input.GetKeyDown(KeyCode.DownArrow))
        {
            hierarchyFlag_ = false;
            audioSource.PlayOneShot(soundMenuSelect_);
        }
        else if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            hierarchyFlag_ = true;
            audioSource.PlayOneShot(soundMenuSelect_);
        }


        

        if ((Input.GetKeyDown(KeyCode.Z) || Input.GetKeyDown(KeyCode.Return))&& hierarchyFlag_ == true)
        {

            
            // バックアップ
            TitleDirector.hpSave_ = status_.hp;
            TitleDirector.exSave_ = status_.experience;
            TitleDirector.atkSave_ = status_.atk;
            TitleDirector.levelSave_ = status_.level;

            ItemCorrection.BagSave(playerItemList_);

            // リザルトに行くか
            if (5 <= TitleDirector.floor_)
            {
                resContent_ = UIManager.Result_Contents.Clear;
//                
                TitleDirector.allEx_ += status_.experience;
                resultFlg_ = true;
               
            }
            else
            {
                if (countFlag_ == false)
                {
                    audioSource.PlayOneShot(soundMenuEnter_);
                    countFlag_ = true;
                }
                
               
            }
        }
        else if ((Input.GetKeyDown(KeyCode.Z) || Input.GetKeyDown(KeyCode.Return)) && hierarchyFlag_ == false)
        {

            audioSource.PlayOneShot(soundMenuCancel_);
            arrow.SetActive(false);
            waitFlg_ = true;
        }

        if (countFlag_ == true)
        {
            seCount_++;
            if (seCount_ > 15)
            {
                TitleDirector.floor_++;
                SceneManager.LoadScene("PlayScene");
            }
        }
    }

    // ------------------ セッター・ゲッター -------------------
    // 引数:フラグ　ターンフラグのセッター
    public void SetFlg(bool flg)
    {
        turnFlg_ = flg;
    }
    // 引数:ダメージ量, タグ名　プレイヤの体力を削る
    public void SetHP(int damage, string tag = null)
    {
        Debug.Log(tag + "ダメージを受けた");
        status_.hp -= damage;
        log_.LogMessage(LogController.LogList.damage, damage);

        //罠を踏んだ時の音
        audioSource.PlayOneShot(enemyDamageSe_);


        if (status_.hp <= 0)
        {
            if (tag == "Enemy")
            {
               

                resContent_ = UIManager.Result_Contents.Enemy;

                status_.hp = 0;
                resultTag_ = tag;
               
                
            }
            else if (tag == "trap")
            {
               
                resContent_ = UIManager.Result_Contents.Trap;

            }
            PlayerDeath();
        }
    }
    
    // 引数:経験値量　経験値取得
    public void SetEX(int experience, string tag)
    {
        log_.LogMessage(LogController.LogList.kill, 0, tag);

        status_.experience += experience;
        log_.LogMessage(LogController.LogList.exp, experience);

        while (TitleDirector.exMax_ <= status_.experience)
        {
            LevelUp();
        }
    }

    // UIが取るゲッター
    public int GetLevel()
    {
        return status_.level;
    }
    public int GetHP()
    {
        return status_.hp;
    }
    public int GetRegain()
    {
        return status_.regain;
    }
    public int GetExperience()
    {
        return status_.experience;
    }


    // 敵にポジションを渡す
    public Vector3 GetPosition()
    {
        return transform.position;
    }

}
