﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAttack : MonoBehaviour
{
    Collider player_;

    void Start()
    {
        player_ = null;
    }


    //public void StartAttack()
    //{
    //    gameObject.layer = LayerMask.NameToLayer("Enemy");
    //}
    //public void EndAttack()
    //{
    //    gameObject.layer = LayerMask.NameToLayer("EnemyAttack"); 
    //}

    // 当たり判定
    void OnTriggerStay(Collider collider)
    {
        if (collider.gameObject.tag == "Player")
        {
            player_ = collider;
        }
    }
    void OnTriggerExit(Collider collider)
    {
        if(collider.gameObject.tag == "Player")
        {
            player_ = null;
        }
    }

    public void SetTarget(Collider collider)
    {
        player_ = collider;
    }
    
    public Collider GetTarget()
    {
        return player_;
    }
}
