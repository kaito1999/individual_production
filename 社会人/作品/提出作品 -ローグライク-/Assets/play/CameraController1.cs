﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// プレイヤから見たカメラの位置
struct CameraPos
{
    public float posX;
    public float posY;
    public float posZ;
    public float rotX;
}

public class CameraController1 : MonoBehaviour
{
    private GameObject player;  // プレイヤオブジェクトを格納
    private Vector3 offset;     // プレイヤとカメラの相対距離
    CameraPos camera_;
    

    void Start()
    {
        // プレハブの名前をfindの引数に入れてください
        this.player = GameObject.Find("animCharacter");

        // カメラの位置、角度を設定
        camera_ = new CameraPos { posX = 0.2f, posY = 3.5f, posZ = -3.0f, rotX = 50.0f };

        transform.position = new Vector3
            (player.transform.position.x + camera_.posX, 
            player.transform.position.y + camera_.posY, 
            player.transform.position.z + camera_.posZ);
        offset = transform.position - player.transform.position;

        transform.rotation = Quaternion.Euler(camera_.rotX, 0, 0);
    }

    void Update()
    {
        transform.position = player.transform.position + offset;
    }
}
