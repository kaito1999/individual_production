﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sound : MonoBehaviour
{
    public PlayerController playerController_;

    public UIManager uiManager_;

    //private  bool trigger_;

    private bool flagCheck_;
    
    private bool flagCheckStop_;

    // Start is called before the first fr4ame update
    void Start()
    {


        GameObject obj = GameObject.Find("animCharacter");
        playerController_ = obj.GetComponent<PlayerController>();

        GameObject uiM_ = GameObject.Find("PlayerUIManager");
        uiManager_ = uiM_.GetComponent<UIManager>();

        //gameObject.GetComponent<AudioSource>().Stop();


        
        //画面遷移してもオブジェクトが壊れないようにする
        DontDestroyOnLoad(this);
            
      
        //連続で流さないようにする
        //trigger_ = false;

         flagCheck_ = false;
        flagCheckStop_ = false;
    }

    // Update is called once per frame
    void Update()
    {
        if (playerController_!= null)
        {
            flagCheck_ = playerController_.loadFlag_;
        }
        // if (flagCheck_ == true)

        //ここでuiManagerから受け取っておく
        if (uiManager_ != null)
        {

            if (TitleDirector.trigger_ == false && flagCheck_ == true)
            {
                gameObject.GetComponent<AudioSource>().Play();
                TitleDirector.trigger_ = true;

            }

            flagCheckStop_ = uiManager_.bgmEndFlag_;


            if (flagCheckStop_ == true)
            {
                gameObject.GetComponent<AudioSource>().Stop();
                TitleDirector.trigger_ = false;
                uiManager_.bgmEndFlag_ = false;
                
            }
        }
       
      
    }
}
