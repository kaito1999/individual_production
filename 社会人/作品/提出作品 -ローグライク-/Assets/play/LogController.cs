﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;　//必須

public class LogController : MonoBehaviour
{
    public GameObject logString_;
    public GameObject logString2_;
    public GameObject logString3_;

    private string LogMess;

    public GameObject LogController_;

    public enum LogList
    {
        attack,
        damage,
        kill,
        exp,
        item,
        use,
        heal,
        levelup,
        hpup,
        attackup,
        suddendeath1,
        suddendeath2,
        suddendeath3,
        suddendeath4,
        trap,
        atkMiss_,
    }

    // 使わない引数を通る時はnullや0を入れて下さい
    // 例 : LogMessage(LogController.LogList.item, 0, null, "リンゴ");
    // 通らない時は大丈夫です
    // 例 : LogMessage(LogController.LogList.attack, 20);


    

    //ログの文字は白文字黒枠で
    public void LogMessage(LogList log, int num = 0, string EnemyName = null, string ItemName = null)
    {
        if (EnemyName == "Enemy")
        {
            EnemyName = "スライム";
        }

        switch (log)
        {
            case LogList.attack: LogMess = EnemyName + "に" + num + "ダメージを与えた！"; break;
            case LogList.damage: LogMess = "プレイヤーは" + num + "ダメージを受けた"; break;
            case LogList.kill: LogMess = EnemyName + "を倒した！"; break;
            case LogList.exp: LogMess = num + "経験値を獲得！"; break;
            case LogList.item: LogMess = ItemName + "を拾った！"; break;
            case LogList.use: LogMess = ItemName + "を使った！"; break;
            case LogList.heal: LogMess = "HPを" + num + "回復した！"; break;
            case LogList.levelup: LogMess = "プレイヤーはレベル" + num + "に上がった！"; break;
            case LogList.hpup: LogMess = "HPが" + num + "上がった！"; break;
            case LogList.attackup: LogMess = "攻撃力が" + num + "上がった！"; break;
            //////////////////////////////////
            case LogList.suddendeath1: LogMess = "遠くで何かが動いたようだ…";break;
            case LogList.suddendeath2: LogMess = "何かが近づいてくる…"; break;
            case LogList.suddendeath3: LogMess = "かなり近づいてきた！"; break;
            case LogList.suddendeath4: LogMess = "突風にさらわれた！"; break;
            case LogList.trap: LogMess = "罠が作動した！！" + num + "ダメージ"; break;
            case LogList.atkMiss_:LogMess = "しかし攻撃は外れてしまった";break;
            /////////////////////////////////
        }
        Debug.Log(LogMess);

        LogController_.GetComponent<LogWindowController>().ShowLogWindow();

        logString3_.GetComponent<Text>().text = logString2_.GetComponent<Text>().text;
        logString2_.GetComponent<Text>().text = logString_.GetComponent<Text>().text;
        logString_.GetComponent<Text>().text = LogMess;


    }

    // Start is called before the first frame update
    void Start()
    {
        //logString_ = GameObject.Find("Log1");
        //logString2_ = GameObject.Find("Log2");
        //logString3_ = GameObject.Find("Log3");

        logString3_.GetComponent<Text>().text = null;
        logString2_.GetComponent<Text>().text = null;
        logString_.GetComponent<Text>().text = null;

    }

}
