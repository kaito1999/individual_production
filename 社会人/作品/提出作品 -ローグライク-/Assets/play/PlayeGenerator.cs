﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayeGenerator : MonoBehaviour
{
    //参照したいprefabの読み込み
    public GameObject animCharacter_ = GameObject.Find("Player");

    //使いたいクラスの代わりとなる変数を作る
    public MapGenerator mapGenerator_;

    // Start is called before the first frame update
    void Start()
    {
        //objにMapGeneratorの能力を入れる
        GameObject obj = GameObject.Find("MapGenerator");
        mapGenerator_ = obj.GetComponent<MapGenerator>();
    }

    // Update is called once per frame
    void Update()
    {
        playeGenerator();
    }

    private void playeGenerator()
    {

        //科のクラス内での代わりの変数
        Vector3 variableCase_;
        //ここでさっき作った変数にマップジェネレーターの   wallSave_の能力を入れる
        variableCase_ = mapGenerator_.wallSave_;
        //                               prefab名
        // GameObject player_ = Instantiate(animCharacter_) as GameObject;

        //これで場所を上書きできるらしいから参考にして
        // transform.position = prePoz;
        //出す場所を決める
        animCharacter_.transform.position = variableCase_;
    }
}
