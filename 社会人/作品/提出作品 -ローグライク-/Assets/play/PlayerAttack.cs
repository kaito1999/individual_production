﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAttack : MonoBehaviour
{
    Collider collider_;
    Collision collision_;

    // 当たり判定
    private void OnTriggerEnter(Collider collider)
    {
        collider_ = collider;
    }
    private void OnTriggerStay(Collider other)
    {
        collider_ = other;
    }
    private void OnTriggerExit(Collider collider)
    {
        collider_ = null;
    }


    // -------------------------ゲッター/セッター-------------------------

    public void SetTarget(Collider collider)
    {
        collider_ = collider;
    }
    public void SetLayer(int layerNum)
    {
        this.gameObject.layer = layerNum;
    }

    public Collider GetTarget()
    {
        return collider_;
    }
    public int GetLayer()
    {
        return this.gameObject.layer;
    }
}
