﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StairsGenerator : MonoBehaviour
{
    //タグの数を数えるときに使う
    GameObject[] tagObjects;

    // 階段プレハブを格納するリスト
    List<GameObject> stairsList_ = new List<GameObject>();
    //
    public GameObject stairPrefab;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        // prefab名ではなくtagを入力する
        Check("Stairs");
        
        stairGenerator();
    }
    private void stairGenerator()
    {
        if(tagObjects.Length == 0)
        {
            GameObject Stair_ = Instantiate(stairPrefab) as GameObject;

            //ランダムで出現させる
            int SpawnX_ = Random.Range(18, -20);
            int SpawnY_ = Random.Range(18, -20);

            //出す場所を決める
            Stair_.transform.position = new Vector3(SpawnX_+0.5f, 2, SpawnY_+0.5f);
        }
    }
    //シーン上のBlockタグが付いたオブジェクトを数える
    void Check(string tagname)
    {
        tagObjects = GameObject.FindGameObjectsWithTag(tagname);

        stairsList_.Clear();
        foreach (GameObject chara in tagObjects)
        {
            stairsList_.Add(chara);
        }

        //現在のタグ付けされたEnemyの数が最大値より少ないときかつ40%の確率で出せるようにした
        //if (ENEMY_MAX > tagObjects.Length )
        //{
        //    stairsCount_ = tagObjects.Length;
        //}


    }
}
