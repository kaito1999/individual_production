﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemGenerator : MonoBehaviour
{


    private int maxItem_;
    public GameObject ItemPrefab;
    public GameObject ItemBigPrefab;

    public int rand_;

    List<GameObject> objList_ = new List<GameObject>();

    List<int> randList_ = new List<int>();

    const int MAX_RAND = 1;
  
    // Start is called before the first frame update
    void Start()
    {
        rand_ = 0;

        maxItem_ = 4;
        //アイテムリストに追加
        {
            //objList_の0番目に挿入
            objList_.Add(ItemPrefab);
            //objList_の1番目に挿入
            objList_.Add(ItemBigPrefab);
        }

        //randListに追加
        {
            //ランダムで0～9まで出して変数に入れる
            //rand_ = Random.Range(1, 0);
            //randList_の0番目に追加
            randList_.Add(Random.Range(0, MAX_RAND));
            Debug.Log("itemのランド0の数値は" + randList_[0] + "だよ!!!!!!!!!!!!!!!!!!!"); //tagObjects.Lengthはオブジェクトの数
            //randList_の1番目に追加
            randList_.Add(Random.Range(MAX_RAND, 0));
            Debug.Log("itemのランド1の数値は" + randList_[1] + "だよ!!!!!!!!!!!!!!!!!!!"); //tagObjects.Lengthはオブジェクトの数
            //randList_の2番目に追加
            randList_.Add(Random.Range(0, MAX_RAND));
            Debug.Log("itemのランド2の数値は" + randList_[2] + "だよ!!!!!!!!!!!!!!!!!!!"); //tagObjects.Lengthはオブジェクトの数
            //randList_の3番目に追加
            randList_.Add(Random.Range(MAX_RAND, 0));
            Debug.Log("itemのランド3の数値は" + randList_[3] + "だよ!!!!!!!!!!!!!!!!!!!"); //tagObjects.Lengthはオブジェクトの数
        }

        //これ最後にしたほういいぞ
        itemGenerator();


    }

    private void itemGenerator()
    {
        //                                  
        for (int i = 0; i < maxItem_; i++)
        {

            GameObject item_ = Instantiate(objList_[randList_[i]]) as GameObject;
            //ランダムで出現させる
            int SpawnX_ = Random.Range(18, -20);
            int SpawnY_ = Random.Range(18, -20);

            //出す場所を決める
            item_.transform.position = new Vector3(SpawnX_ + 0.5f, 2, SpawnY_ + 0.5f);
        }
    }
}
